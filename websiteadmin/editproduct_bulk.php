<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
	 
	 //======== College photo =======================
	if($_FILES["usrphoto"]["size"]>0 && $_FILES["usrphoto"]["name"] !=" ") 
	{
		//=========== to upload the file =============		
		$photofile_name="";
		$uploaddir="../product_photo/";
		$uploadfrom = $_FILES["usrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["usrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $photofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$photofile_name)){
			  $photofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$photofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$photofile_name=$_REQUEST["smimg"];
	}
	
	
	//======== College photo =======================
	
	        
	$ins_emp = "update tbl_subcategory_bulk set
				maincat_id 		= '".$_REQUEST["selcategory"]."',
				category_id 	= '".$_REQUEST["selsubcat"]."',
				product_code 	= '".$_REQUEST["prdcode"]."',
				product_name 	= '".addslashes($_REQUEST["prdname"])."',
				manufacture_name= '".addslashes($_REQUEST["selmanu"])."',
				rate 			= '".$_REQUEST["prdrate"]."',
				previous_rate 	= '".$_REQUEST["prdrateold"]."',
				descp 			= '".addslashes($_REQUEST["prddet"])."',
				specification 	= '".addslashes($_REQUEST["prddets"])."',
				manufacture_warranty = '".$_REQUEST["manuwarranty"]."',
				replace_warranty 	= '".$_REQUEST["replwarranty"]."',
				publishedby 	= '".$_REQUEST["pubby"]."',
				image			= '".$photofile_name."',
				is_featured		= '".$_REQUEST["fprd"]."',
				stock_status		= '".$_REQUEST["statusstock"]."',
				subcat_status	= '".$_REQUEST["catsta"]."',
				preorder_status	= '".$_REQUEST["prestatus"]."'
				where subcat_id='".$_REQUEST["cid"]."' ";
				
			/*echo "<pre>";
			print_r($ins_emp);     	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		url_redirect("mng_bulk_products.php?edit=2");
	}
}

$sqlapp = "select * from tbl_subcategory_bulk where subcat_id='".$_REQUEST["cid"]."' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();

require '../fckeditor/fckeditor.php';
$oFCKeditor = new FCKeditor('prddet') ;
$oFCKeditor->BasePath	= '../fckeditor/' ;
$oFCKeditor->Width = '620px';
$oFCKeditor->Height = '500px';
$oFCKeditor->Value		=stripslashes($resapp->fields["descp"]);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	    //var selcategory=trim(document.frmcrm.selcategory.value);
	//var selsubcat=trim(document.frmcrm.selsubcat.value)
	//var selmanu=trim(document.frmcrm.selmanu.value);
	var prdcode=trim(document.frmcrm.prdcode.value);
	var prdname=trim(document.frmcrm.prdname.value);
	var prdrate=trim(document.frmcrm.prdrate.value);
	var usrphoto=trim(document.frmcrm.usrphoto.value);
	//var bigusrphoto=trim(document.frmcrm.bigusrphoto.value);
	//var username=trim(document.frmcrm.username.value);
	//var newpass=trim(document.frmcrm.newpass.value);
	//var confpass=trim(document.frmcrm.confpass.value);
	
	/*if(selcategory=="")
	{
		alert('Please select category.');
		document.frmcrm.selcategory.focus();
		return false;
	}
	else if(selsubcat=="")
	{
		alert('Please select subcategory.');
		document.frmcrm.selsubcat.focus();
		return false;
	}
	else if(selmanu=="")
	{
		alert('Please select manufacturer Name.');
		document.frmcrm.selmanu.focus();
		return false;
	}
	else */
	if(prdcode=="")
	{
		alert('Product code should not be blank.');
		document.frmcrm.prdcode.focus();
		return false;
	}
	else if(prdname=="")
	{
		alert('Product Name should not be blank .');
		document.frmcrm.prdname.focus();
		return false;
	}
	else if(prdrate=="")
	{
		alert('Product rate should not be blank .');
		document.frmcrm.prdrate.focus();
		return false;
	}
	else if(isNaN(prdrate))
	{
		alert('Product rate should be number only.');
		document.frmcrm.prdrate.focus();
		return false;
	}
	
	else if(usrphoto!=="")
	{
		var ss;
		ss=1 + usrphoto.lastIndexOf(".");
		var ext =usrphoto.substr(ss);
		ext=ext.toLowerCase();
		if (!(ext == "gif" || ext == "jpeg"|| ext == "jpg"))
		{		
			alert('You can upload only doc or pdf file.');
			document.frmcrm.usrphoto.value="";
			document.frmcrm.usrphoto.focus();
			return false;
		}
	}
	
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Update Bulk Product</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />
<input type="hidden" name="smimg" id="smimg" value="<?php echo $resapp->fields["image"];?>" />
<input type="hidden" name="bgimg" id="bgimg" value="<?php echo $resapp->fields["big_img"];?>" />
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td valign="top">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Product Details</legend>
    <table border="0" width="95%" style="background-image:none; border:none;">
    <tr style="display:none;">
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Select Category : </b></label></td>
    <td width="70%">
    <?php
        $sql_ct = "select * from  tbl_category_main where status='0' order by cat_name";
        $resct = $db->Execute($sql_ct);
        $total_ct  = $resct->RecordCount();
        ?>
        <select name="selcategory" id="selcategory" class="widsel" onchange="fillsubcat(this.value);">
        <option value="">- Select Category -</option>
        <?php 
        if($total_ct>0){
        while (!$resct->EOF) {		
        ?>
        <option value="<?php echo $resct->fields["catid"];?>" <?php if($resct->fields["catid"]==$resapp->fields["maincat_id"]) { echo 'selected="selected"';}?>><?php echo $resct->fields["cat_name"];?></option>
        <?php
        $resct->MoveNext();
        }
        }?>
        </select>
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Select Subcategory : </b></label></td>
    <td>
    <div id="selscat">
	<?php
    $sql_ct = "select * from tbl_category where maincat_id='".$resapp->fields["maincat_id"]."' order by cat_name";
    $resct = $db->Execute($sql_ct);
    $total_ct  = $resct->RecordCount();
    ?>
    <select name="selsubcat" id="selsubcat" class="widsel">
    <option value="">- Select Subcategory -</option>
    <?php 
    if($total_ct>0){
    while (!$resct->EOF) {		
    ?>
    <option value="<?php echo $resct->fields["catid"];?>" <?php if($resct->fields["catid"]==$resapp->fields["category_id"]) { echo 'selected="selected"';}?>><?php echo $resct->fields["cat_name"];?></option>
    <?php
    $resct->MoveNext();
    }
    }?>
    </select>
    </div>
    </td>
    </tr> 
    <tr style="display:none;">
    <td align="right"><label><b><!--<span class="requfield">*</span>-->Select Manufacture : </b></label></td>
    <td>
    <?php
        $sql_ct = "select * from  tbl_manufacture_details where status='0' order by manufacture_name";
        $resct = $db->Execute($sql_ct);
        $total_ct  = $resct->RecordCount();
        ?>
        <select name="selmanu" id="selmanu" class="widsel">
        <option value="">- Select Manufacture -</option>
        <?php 
        if($total_ct>0){
        while (!$resct->EOF) {		
        ?>
        <option value="<?php echo $resct->fields["manu_id"];?>" <?php if($resct->fields["manu_id"]==$resapp->fields["manufacture_name"]) { echo 'selected="selected"';}?>><?php echo $resct->fields["manufacture_name"];?></option>
        <?php
        $resct->MoveNext();
        }
        }?>
        </select>
    </td>
    </tr>
     <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Product Code : </b></label></td>
    <td width="70%"><input type="text"  name="prdcode" id="prdcode"  value="<?php echo $resapp->fields["product_code"];?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Product Name : </b></label></td>
    <td width="70%"><input type="text"  name="prdname" id="prdname"  value="<?php echo $resapp->fields["product_name"];?>" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b>Previous Rate : </b></label></td>
    <td><input type="text"  name="prdrateold" id="prdrateold"  value="<?php echo $resapp->fields["previous_rate"];?>" class="widtxt" /></td>
    </tr>
     <tr>
    <td align="right"><label><b><span class="requfield">*</span>Rate : </b></label></td>
    <td><input type="text"  name="prdrate" id="prdrate"  value="<?php echo $resapp->fields["rate"];?>" class="widtxt" /></td>
    </tr>
     <tr>
    <td align="right" valign="top"><label><b><span class="requfield">*</span>Product Description : </b></label></td>
    <td>
    <?php echo $oFCKeditor->Create();?>
    <!--<textarea name="prddet" id="prddet" rows="5" cols="40" class="mini"><?php //echo $resapp->fields["descp"];?></textarea>-->
    </td>
    </tr>
     
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Manufacture Warranty : </b></label></td>
    <td width="70%"><input type="text"  name="manuwarranty" id="manuwarranty"  value="<?php echo $resapp->fields["manufacture_warranty"];?>" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Replacement Warranty : </b></label></td>
    <td width="70%"><input type="text"  name="replwarranty" id="replwarranty"  value="<?php echo $resapp->fields["replace_warranty"];?>" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Published By : </b></label></td>
    <td width="70%"><input type="text"  name="pubby" id="pubby"  value="<?php echo $resapp->fields["publishedby"];?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Small Image (212x212) : </b></label></td>
    <td><input type="file" name="usrphoto" id="usrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>
   
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Product Status : </b></label></td>
    <td>
    <input type="radio" name="catsta" id="catsta" <?php if($resapp->fields["subcat_status"]=="0") { echo 'checked="checked"';}?> value="0" />Active&nbsp;
    <input type="radio" name="catsta" id="catsta" <?php if($resapp->fields["subcat_status"]=="1") { echo 'checked="checked"';}?> value="1" />Inactive&nbsp;
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Featured Product : </b></label></td>
    <td>
    <input type="radio" name="fprd" id="fprd" <?php if($resapp->fields["is_featured"]=="0") { echo 'checked="checked"';}?> value="0" />Not a Feature&nbsp;
    <input type="radio" name="fprd" id="fprd" <?php if($resapp->fields["is_featured"]=="1") { echo 'checked="checked"';}?> value="1" />Feature&nbsp; <input type="radio" name="fprd" id="fprd" <?php if($resapp->fields["is_featured"]=="2") { echo 'checked="checked"';}?> value="2" />New Product&nbsp;
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Stock Status : </b></label></td> 
    <td>
    <input type="radio" name="statusstock" id="statusstock" <?php if($resapp->fields["stock_status"]=="0") { echo 'checked="checked"';}?> value="0" />In Stock&nbsp;
    <input type="radio" name="statusstock" id="statusstock" <?php if($resapp->fields["stock_status"]=="1") { echo 'checked="checked"';}?> value="1" />Out Stock&nbsp;
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Preorder Status : </b></label></td> 
    <td>
    <input type="radio" name="prestatus" id="prestatus" <?php if($resapp->fields["preorder_status"]=="0") { echo 'checked="checked"';}?> value="0" />Normal Product&nbsp;
    <input type="radio" name="prestatus" id="prestatus" <?php if($resapp->fields["preorder_status"]=="1") { echo 'checked="checked"';}?> value="1" />Preorder Product&nbsp;
    </td>
    </tr>
    <tr>
    <td colspan="2" height="20"></td>
    </tr>
    <tr>
    <td align="right">&nbsp;</td>
    <td>
    <input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" />
    </td>
    </tr>  
    <tr>
    <td align="center"><b>Small Image</b></td>
    <td align="left"><?php if(!empty($resapp->fields["image"])){?>
    <img src="<?php echo "../product_photo/".$resapp->fields["image"];?>" border="0" width="100" height="100" />
    <?php }?></td>
    </tr> 
   
    </table>
    </fieldset>
    </td>
  </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>