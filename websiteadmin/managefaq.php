<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();


$delmsg="";

if(isset($_REQUEST["act"]) && $_REQUEST["act"]=="delete")
{
	
	$sqldel=" delete from  tbl_subcategory where  subcat_id ='".$_REQUEST["cid"]."' ";
	$rowdel = $db->Execute($sqldel);
	
	if($rowdel)
	{
		$delmsg=" Selected product has been deleted.";
	}
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function view_large_image(id,width,height)
{
	
	//window.open("view_large_image.php?product_id="+product_id,"product_id","width=740,height=400,scrollbars=no");
	 var left = (screen.width/2)-(width/2);
	var top = (screen.height/2)-(height/2);
	imgview=window.open("view_large_image.php?id="+id,"Product","width="+width+"px,height="+height+"px,scrollbars=yes,top="+top+", left="+left);
	if(window.focus)
	{
		imgview.focus();
	}
	
}


function validform()
{
	                 
	//var fromdt=document.getElementById('datepicker').value;
	//var todt=document.getElementById('datepicker1').value;
	var srcval=document.getElementById('txtsrc').value;	
	//var selmanu=document.getElementById('selmanu').value;
	search_frdajax(srcval,'1');
}

function getpage(srcval,page)
{
	search_frdajax(srcval,page);

}



//==== Validate form filled by user ================== 
function checkusrval()
{
	//alert('qwqe');
	var usrname=trim(document.frmadmlog.usrname.value);
	var usrpass=trim(document.frmadmlog.usrpass.value);
	
	if(usrname=="")
	{
		alert("User name should not be blank.");
		document.frmadmlog.usrname.focus();
		return false;
	}	
	else if(usrpass=="")
	{
		alert("Password should not be blank.");
		document.frmadmlog.usrpass.focus();
		return false;
	}
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	


function viewprd(incid)
{ 
	imgview=window.open("prodview.php?incid="+incid,"Details",'height=500,width=850,resizable=1,scrollbars=1,screenX=200,screenY=100, menubar=1,fullscreen=1');
	
	if(window.focus)
	{
		imgview.focus();
	}
}
</script>          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Manage FAQs</h2>
                <div class="block">
                <?php if(isset($_REQUEST["add"]) && $_REQUEST["add"]=="1"){?>
                <div class="message success"><p><b>FAQs has been added successfully.</b></p></div>
                <?php }?>
                 <?php if(isset($_REQUEST["edit"]) && $_REQUEST["edit"]=="2"){?>
                <div class="message success"><p><b>FAQs has been updated successfully.</b></p></div>
                <?php }?>
                
                <?php if(isset($delmsg) && !empty($delmsg)){?>
                <div class="message success"><p><b><?php echo $delmsg;?></b></p></div>
                <?php }?>
                
                <!-- Table data start-->
                <div style=" font-weight:bold;">
                 Search Keyword&nbsp;&nbsp;<input type="text" name="txtsrc" id="txtsrc" value="" class="long" style="width:300px;" />
                
                 &nbsp;&nbsp;<input type="button" name="btnsrc" id="btnsrc" onclick="validform();" value="Search" /><br />
               <!--<span style="color:#666;">(Keyword Search by name,phone no,email,account no)</span>-->
               <br /><br />
               </div>
               <div id="srcresult">
        
                </div>
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
<script language="javascript">
search_frdajax('','1');
</script>
</body>
</html>