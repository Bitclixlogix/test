<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	

$fromdt=$_REQUEST["fromdt"];
$todt=$_REQUEST["todt"];
$srcval=$_REQUEST["srcval"];

$targetpage = "'".$fromdt."','".$todt."','".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from tbl_note_details where cust_id !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and note_details like '%".$srcval."%' ";
}

if($fromdt!="" && $todt=="")
{
	$sqlsrc.= " and added_date >='".date("Y-m-d",strtotime($fromdt))."' ";
}

if($todt!="" && $fromdt=="")
{
	$sqlsrc.= " and added_date <='".$todt."' ";
}

if($fromdt!="" && $todt!="")
{
	$sqlsrc.= " and office_city ='".$cityname."' ";
}


$sqlsrc .= " and addedby='".$_SESSION['uniqID']."' order by added_date desc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from tbl_note_details where cust_id !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and note_details like '%".$srcval."%' ";
}

if($fromdt!="" && $todt=="")
{
	$sqltot.= " and added_date >='".date("Y-m-d",strtotime($fromdt))."' ";
}

if($todt!="" && $fromdt=="")
{
	$sqltot.= " and added_date <='".$todt."' ";
}

if($fromdt!="" && $todt!="")
{
	$sqltot.= " and office_city ='".$cityname."' ";
}


$sqltot .= " and addedby='".$_SESSION['uniqID']."' order by added_date desc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#999999">
	<th width="10%" align="center">Date</th>
    <th height="25" width="10%" align="center">Account No.</th>
    <th width="20%" align="left">Name</th>    
    <th width="60%">Notes</th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
	<td align="center"><?php echo date("d-m-Y",strtotime($row_src->fields["added_date"]));?></td>
    <td align="center">
    <a href="javascript:void(0);" onClick="viewemployee('<?php echo $row_src->fields["cust_id"];?>');"><?php echo show_accountno($row_src->fields["cust_id"]);?></a>
    </td>
    <td align="left"><?php echo show_name($row_src->fields["cust_id"]);?></td>    
    <td><?php echo $row_src->fields["note_details"];?></td>
    
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="4" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="4" align="center" height="25"><b>No Note Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>