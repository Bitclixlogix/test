<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
		 
	$ins_emp = "insert into tbl_plan_details set				
				cust_id				= '".$_REQUEST["cid"]."',
				plan_no				= '".$_REQUEST["plandet"]."',
				tenure				= '".$_REQUEST["seltenure"]."',
				devices				= '".$_REQUEST["seldevice"]."',
				amount				= '".$_REQUEST["amounttk"]."',
				order_no			= '".$_REQUEST["orderno"]."',
				tenure_start_date	= '".date("Y-m-d",strtotime($_REQUEST["startdt"]))."',
				tenure_end_date		= '".date("Y-m-d",strtotime($_REQUEST["dojdt"]))."',
				plan_added_date		= '".date("Y-m-d")."',
				plan_addedby		= '".$_SESSION['uniqID']."'";
				
			/*echo "<pre>";
			print_r($ins_emp);    	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		url_redirect("addplan.php?add=1&cid=".$_REQUEST["cid"]);
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	        
	 
	var plandet=trim(document.frmcrm.plandet.value);
	var seldevice=trim(document.frmcrm.seldevice.value);
	var amounttk=trim(document.frmcrm.amounttk.value);
	var orderno=trim(document.frmcrm.orderno.value);
	//var residntstate=trim(document.frmcrm.residntstate.value);
	//var callsource=trim(document.frmcrm.callsource.value);
	
	if(plandet=="")
	{
		alert('Plan should not be blank.');
		document.frmcrm.plandet.focus();
		return false;
	}
	else if(seldevice=="")
	{
		alert('Please select no of device.');
		document.frmcrm.seldevice.focus();
		return false;
	}
	else if(amounttk=="")
	{
		alert('Amount should not be blank.');
		document.frmcrm.amounttk.focus();
		return false;
	}
	else if(isNaN(amounttk))
	{
		alert('Amount should be only Number.');
		document.frmcrm.amounttk.focus();
		return false;
	}
	else if(orderno=="")
	{
		alert('Order number should not be blank.');
		document.frmcrm.orderno.focus();
		return false;
	}
	/*else if(residntstate=="")
	{
		alert('Please select state.');
		document.frmcrm.residntstate.focus();
		return false;
	}
	else if(callsource=="")
	{
		alert('Please select Call Source.');
		document.frmcrm.callsource.focus();
		return false;
	}*/
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker2").datepicker({minDate: -500, maxDate: +500});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -500, maxDate: +500});
	});	



</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Add New Plan</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <?php if($_REQUEST["add"]=="1"){?>
 <tr bgcolor="#D2EACC">
 <td colspan="2" align="center" height="25"><b>Plan has been added successfully.</b></td>
 </tr>
 <?php }?>
 <tr>
    <td valign="top" width="50%">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Add Plan</legend>
    <table border="0" width="95%" style="background-image:none; border:none;" cellpadding="5" cellspacing="0">
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Plan No. : </b></label></td>
    <td width="70%">
    <?php
	$sqlnx = "select * from tbl_plan_details where cust_id ='".$_REQUEST["cid"]."' ";
	$resnx = $db->Execute($sqlnx);
	$totalnx  = $resnx->RecordCount();
	
	$nextplan="Plan ".($totalnx+1);
	?>
    <input type="text"  name="plandet" id="plandet"  value="<?php echo $nextplan;?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b>Tenure : </b></label></td>
    <td>
    <select name="seltenure" id="seltenure" class="widsel" onchange="monthchng(this.value);">
    <option value="">Select Tenure</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    </select>  
    </td>
    </tr>  
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Devices : </b></label></td>
    <td>
     <select name="seldevice" id="seldevice" class="widsel">
    <option value="">Select Devices</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    </select>  
    </td>
    </tr>   
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Amount : </b></label></td>
    <td><input type="text"  name="amounttk" id="amounttk" value="" class="widtxt" /></td>
    </tr>    
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Order No. : </b></label></td>
    <td><input type="text"  name="orderno" id="orderno"  value="" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b>Tenure Start Date : </b></label></td>
    <td><input type="text" name="startdt" value="<?php echo date("m/d/Y");?>" class="dp-choose-date" id="datepicker2" readonly="readonly" style="width:150px; background-color:#F0F0F0;" ></td>
    </tr>
    <tr>
    <td align="right" width="40%"><label><b>Tenure End Date : </b></label></td>
    <td width="60%"><input type="text" name="dojdt" value="" class="dp-choose-date" id="datepicker1" readonly="readonly" style="width:150px; background-color:#F0F0F0;" ></td> 
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td><input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" /></td>
    </tr>
    <tr>
    <td colspan="2" height="5"></td>
    </tr>
    </table>
    </fieldset>
    
    </td>
    <td valign="top" width="50%">
<?php
$sqlapp = "select * from tbl_newcall_details where new_id='".$_REQUEST["cid"]."' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();
?>
     <fieldset style="border:1px solid #CCC; padding-left:10px; padding-bottom:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Account Details</legend>
        <table border="0" width="95%" style="background-image:none; border:none;" cellpadding="3" cellspacing="0">
    
    <tr>
    <td align="right" width="30%"><label><b>Account No. : </b></label></td>
    <td width="70%"><?php echo $resapp->fields["account_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Name : </b></label></td>
    <td><?php echo $resapp->fields["title"]." ".$resapp->fields["fname"]." ".$resapp->fields["lname"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Email : </b></label></td>
    <td><?php echo $resapp->fields["email_id"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Home No. : </b></label></td>
    <td><?php echo $resapp->fields["home_no"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Cell No. : </b></label></td>
    <td><?php echo $resapp->fields["cell_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Call Source : </b></label></td>
    <td><?php echo $resapp->fields["call_source"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Country : </b></label></td>
    <td><?php 
if($resapp->fields["selcountry"]=="1")
{
	echo "United State";
}
else
{
	echo "Canada";
}
?></td>
    </tr>
    <tr>
    <td align="right"><label><b>State : </b></label></td>
    <td><?php echo showcuststate($resapp->fields["selstate"]);?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Zip Code : </b></label></td>
    <td><?php echo $resapp->fields["zipcode"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>City : </b></label></td>
    <td><?php echo $resapp->fields["city_name"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Time Zone: </b></label></td>
    <td><?php echo $resapp->fields["time_zone"];?>  
    </td>
    </tr>
    </table>
    </fieldset>
</td>
  </tr>
  <tr>
  <tr>
  <td colspan="2">
	<?php
    $sqlpl = "select * from tbl_plan_details where cust_id='".$_REQUEST["cid"]."' ";
    $respl = $db->Execute($sqlpl);
    $totalpl  = $respl->RecordCount();	
    ?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#CCCCCC">
<td colspan="8"><b>Plan Deails</b></td>
</tr>
<tr bgcolor="#999999">
<th height="25" width="20%" align="center">Plan</th>
<th width="8%">Tenure</th>
<th width="8%" align="center">Device</th>
<th width="14%" align="center">Amount</th>
<th width="10%" align="center">Order No.</th>
<th width="10%" align="center">Start Date</th>
<th width="10%" align="center">End Date</th>
<th width="20%" align="center">Added By</th>
</tr>
<?php
if($totalpl>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$respl->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td><?php echo $respl->fields["plan_no"];?></td>
<td align="center"><?php echo $respl->fields["tenure"];?></td>
<td align="center"><?php echo $respl->fields["devices"];?></td>
<td align="center"><?php echo $respl->fields["amount"];?></td>
<td align="center"><?php echo $respl->fields["order_no"];?></td>
<td align="center"><?php echo date("d-m-Y",strtotime($respl->fields["tenure_start_date"]));?></td>
<td align="center"><?php echo date("d-m-Y",strtotime($respl->fields["tenure_end_date"]));?></td>
<td align="center"><?php echo show_empname($respl->fields["plan_addedby"]);?></td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$respl->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="8" align="center" height="25"><b>No Plan Details found.</b></td>
</tr>
<?php }?>
</table>
  </td>
  </tr>  
 </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>