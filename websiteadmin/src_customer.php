<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	

$fromdt=$_REQUEST["fromdt"];
$todt=$_REQUEST["todt"];
$srcval=$_REQUEST["srcval"];

$targetpage = "'".$fromdt."','".$todt."','".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from tbl_newcall_details where account_no !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (account_no ='".$srcval."' or fname like '%".$srcval."%' or lname like '%".$srcval."%' or email_id like '%".$srcval."%' or home_no like '%".$srcval."%') ";
}

if($fromdt!="" && $todt=="")
{
	$sqlsrc.= " and added_date >='".date("Y-m-d",strtotime($fromdt))."' ";
}

if($todt!="" && $fromdt=="")
{
	$sqlsrc.= " and added_date <='".$todt."' ";
}

if($fromdt!="" && $todt!="")
{
	$sqlsrc.= " and office_city ='".$cityname."' ";
}


$sqlsrc .= " order by added_date desc,fname asc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from tbl_newcall_details where account_no !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (account_no ='".$srcval."' or fname like '%".$srcval."%' or lname like '%".$srcval."%' or email_id like '%".$srcval."%' or home_no like '%".$srcval."%') ";
}

if($fromdt!="" && $todt=="")
{
	$sqltot.= " and added_date >='".date("Y-m-d",strtotime($fromdt))."' ";
}

if($todt!="" && $fromdt=="")
{
	$sqltot.= " and added_date <='".$todt."' ";
}

if($fromdt!="" && $todt!="")
{
	$sqltot.= " and office_city ='".$cityname."' ";
}


$sqltot .= " order by added_date desc,fname asc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="7" align="right"><a href="addnewcall.php">Add New Customer</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
    <th height="25" width="9%" align="center">Account No.</th>
    <th width="15%">Name</th>
    <th width="10%" align="center">Contact No</th>
    <th width="23%">Email</th>
    <th width="11%" align="center">State</th>
    <th width="11%" align="center">Payment Status</th>
    <th width="21%" align="center">Action</th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
    <td align="center">
    <a href="callingdash.php?cid=<?php echo $row_src->fields["new_id"];?>"><?php echo $row_src->fields["account_no"];?></a>
    </td>
    <td><a href="javascript:void(0);" onClick="viewemployee('<?php echo $row_src->fields["new_id"];?>');"><?php echo $row_src->fields["title"]." ".$row_src->fields["fname"]." ".$row_src->fields["lname"];?></a></td>
    <td align="center"><?php echo $row_src->fields["home_no"];?></td>
    <td><?php echo $row_src->fields["email_id"];?></td>
    <td align="center"><?php echo showcuststate($row_src->fields["selstate"]);?></td>
    <td align="center">
    <?php if($row_src->fields["payment_status"]=='0'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Not Approved</span>
    <?php } else if($row_src->fields["payment_status"]=='2') {?>
     <span style="color:#F60; font-weight:bold; text-align:center;">Refund</span>
	<?php } else {?>
    <span style="color:#030; font-weight:bold; text-align:center;">Approved</span>
    <?php }?>
    </td>
    <td align="center">
    <a href="addnote.php?cid=<?php echo $row_src->fields["new_id"];?>">+Note</a>
    <?php if($row_src->fields["payment_status"]=='2'){?>
    <?php } else {?>
	<?php if($row_src->fields["voice_status"]=="6"){?>
    &nbsp;&nbsp;<a href="addplan.php?cid=<?php echo $row_src->fields["new_id"];?>">+Plan</a>
    <?php }?>
    <?php if($row_src->fields["payment_status"]=='1'){?>
    &nbsp;&nbsp;<a href="adddevice.php?cid=<?php echo $row_src->fields["new_id"];?>">+Device</a>
    &nbsp;&nbsp;<a href="addtickets.php?cid=<?php echo $row_src->fields["new_id"];?>">+Ticket</a>
    <?php }?>
    <?php }?>
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="7" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="7" align="center" height="25"><b>No Customer Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>