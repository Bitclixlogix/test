<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

$msgres="";
if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Update")
{
	      

		$ins_emp = "update tbl_socialmedia_link set
				   facebook_url   ='".$_REQUEST["facebooklnk"]."',
				   twitter_url    ='".$_REQUEST["twitterlnk"]."',
				   instagram_url  ='".$_REQUEST["instalink"]."',
				   youtube_url    ='".$_REQUEST["youtubelink"]."' where social_id ='1' ";
				
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
		if($ins_result)
		{
			$msgres='Social Link has been change successfully.';
		}
		
	
}

$sqlapp = "select * from tbl_socialmedia_link where social_id='1' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	         
	 
	var facebooklnk=trim(document.frmcrm.facebooklnk.value);
	var twitterlnk=trim(document.frmcrm.twitterlnk.value);
	var instalink=trim(document.frmcrm.instalink.value);
	var youtubelink=trim(document.frmcrm.youtubelink.value);
	//var confpass=trim(document.frmcrm.confpass.value);
	
	
	if(facebooklnk=="")
	{
		alert('Facebook Link should not be blank .');
		document.frmcrm.facebooklnk.focus();
		return false;
	}
	else if(twitterlnk=="")
	{
		alert('Twitter Link should not be blank.');
		document.frmcrm.twitterlnk.focus();
		return false;
	}
	else if(instalink=="")
	{
		alert('Instagram Link should not be blank.');
		document.frmcrm.instalink.focus();
		return false;
	}
	else if(youtubelink=="")
	{
		alert('Youtube Link should not be blank.');
		document.frmcrm.youtubelink.focus();
		return false;
	}

}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Social Media Link</h2>
                <div class="block">
                <!-- Table data start-->
                 <?php if(isset($msgres) && !empty($msgres)) {?>
                <div style="text-align:center; color:#FF0000; font-weight:bold;">
               <?php echo $msgres;?>
                </div>
                 <?php }?> 
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td valign="top">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Social Media Link</legend>
    <table border="0" width="95%" style="background-image:none; border:none;">
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Facebook Link : </b></label></td>
    <td width="70%"><input type="text"  name="facebooklnk" id="facebooklnk" style="width:500px;"  value="<?php echo $resapp->fields["facebook_url"];?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Twitter Link : </b></label></td>
    <td><input type="text"  name="twitterlnk" id="twitterlnk" style="width:500px;"  value="<?php echo $resapp->fields["twitter_url"];?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Instagram Link : </b></label></td>
    <td><input type="text"  name="instalink" id="instalink"  style="width:500px;" value="<?php echo $resapp->fields["instagram_url"];?>" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Youtube Link : </b></label></td>
    <td><input type="text"  name="youtubelink" id="youtubelink" style="width:500px;"  value="<?php echo $resapp->fields["youtube_url"];?>" class="widtxt" /></td>
    </tr>
    <tr><td colspan="2" height="20"></td></tr> 
    <tr>
    <td></td>
    <td  align="center">
    <!--<a href="add_device.php">-->
    <input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Update" />
    <!--</a>-->
    </td>
  </tr>
    </table>
    </fieldset>
    </td>
  </tr>
  
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>