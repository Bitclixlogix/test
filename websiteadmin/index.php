<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
$invalid_msg="";
if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Login")
{
	$username=$_REQUEST["usrname"];
	$password=$_REQUEST["usrpass"];
		
		
	$usr_qry = "select * from tbl_adminuser where username='".$username."' and password='".md5($password)."' ";
	$result = $db->Execute($usr_qry);
	$total_usr  = $result->RecordCount();
	
	//die;
	
	if($total_usr>0)
	{
					
		//======== Login user id store in the session ====		
		$_SESSION['uniqID'] = $result->fields['id'];
		$_SESSION['username'] = $result->fields['name'];
		$_SESSION['usertype'] = $result->fields['user_type'];
		
		if($result->fields['user_type']=="1")
		{
			url_redirect('welcome.php');
		}
		else
		{
			url_redirect('serviceboard.php');
		}
						
	}	
	else
	{
		$invalid_msg="Invalid username/password. Please try again!";
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function checkusrval()
{
	//alert('qwqe');
	var usrname=trim(document.frmadmlog.usrname.value);
	var usrpass=trim(document.frmadmlog.usrpass.value);
	
	if(usrname=="")
	{
		alert("User name should not be blank.");
		document.frmadmlog.usrname.focus();
		return false;
	}	
	else if(usrpass=="")
	{
		alert("Password should not be blank.");
		document.frmadmlog.usrpass.focus();
		return false;
	}
	
}
</script>          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
            <div id="branding">
                <div class="floatleft">
                    <img src="img/logo.png" alt="Logo" /></div>
                <!--<div class="floatright">
                    <div class="floatleft">
                        <img src="img/img-profile.jpg" alt="Profile Pic" /></div>
                    <div class="floatleft marginleft10">
                        <ul class="inline-ul floatleft">
                            <li>Hello Admin</li>
                            <li><a href="#">Config</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>
                        <br />
                        <span class="small grey">Last Login: 3 hours ago</span>
                    </div>
                </div>-->
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="grid_12">
      <br /><br /><br />
		<div style="padding-left:400px;">
        <form name="frmadmlog" id="frmadmlog"  action="" method="post" onSubmit="return checkusrval();">
        <div class="box round " style="width:500px; background-color:#FFF; height:300px;">
        <h2>Employee Login</h2> 
            <?php if(isset($invalid_msg) && !empty($invalid_msg)) {?>
            <br />
            <div class="message error">
             <p><?php echo $invalid_msg;?></p>
            </div>
            <?php }?>
            <div style="width:200px; float:left; padding-top:20px;background-color:#FFF;">
            <img src="img/loginimg.jpg" align="left" border="0" />
            </div>
            <div style="width:300px; float:left; border:0px solid #F00; background-color:#FFF; line-height:25px; text-align:left;">
                
                 <table width="100%" border="0" cellspacing="0" cellpadding="5" style="border:0px solid #CCC; border-collapse:collapse;">
                 <tr>
                 <td height="10"></td>
                 </tr>
                 <tr>                   
                    <td><label><b>Username<span style="color:#F00">*</span></b></label><br />
                <input type="text" name="usrname" id="usrname" value="" class="long" /></td>
                    </tr>
                    <tr>
                    <td><label><b>Password<span style="color:#F00">*</span></b></label><br />
                      <input type="password" id="usrpass" name="usrpass" class="long" /></td>
                    </tr>
                    <tr>
                 <td height="5"></td>
                 </tr>
                    <tr>
                    <td><input type="submit" name="btnsubmit" id="btnsubmit" style="font-weight:bold; padding:4px;" value="Login" /></td>
                    </tr>
                    </table>

            
            </div>          
        </div>
        </form>
        </div>
 <br /><br /><br /><br /><br /><br /><br />
                    

        </div>        
        <div class="clear"></div>
    </div>
    <div class="clear">
    </div>
    <div id="site_info">
        <p>
            Copyright <a href="#">GTM Infotech</a>. All Rights Reserved.
        </p>
    </div>
</body>
</html>