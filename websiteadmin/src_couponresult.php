<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_coupon_details where coupon_id !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (user_name  ='".$srcval."' or email_id  ='".$srcval."' or coupon_code  ='".$srcval."' ) ";
}


$sqlsrc .= " order by created_date desc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_coupon_details where coupon_id !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (user_name  ='".$srcval."' or email_id  ='".$srcval."' or coupon_code  ='".$srcval."' ) ";	
}

$sqltot .= " order by created_date desc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="7" align="right"><a href="addcoupon.php">Add New Coupon</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
<th width="15%" align="left">Name</th>
<th width="27%" align="left">Email</th>
<th width="12%"><center>Mobile No.</center></th>
<th width="12%"><center>Coupon Code</center></th>
<th width="10%"><center>Amount</center></th>
<th width="14%"><center>Status</center></th>
<!--<th width="15%" align="center">Order</th>-->
<th width="10%"><center>Action</center></th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"'; 
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
    <td align="left"><?php echo $row_src->fields["user_name"];?></td>
    <td align="left"><?php echo $row_src->fields["email_id"];?></td>
    <td align="center"><?php echo $row_src->fields["mobile_no"];?></td>
    <td align="center"><?php echo $row_src->fields["coupon_code"];?></td>
    <td align="center"><?php echo $row_src->fields["coupon_amount"];?></td>
    <td align="center">
	<?php if($row_src->fields["coupon_status"]=='1'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Used</span>
    <?php } else {?>
    <span style="color:#030; font-weight:bold; text-align:center;">Not Used</span>
    <?php }?>
    </td>
    <td align="center">
   <!-- <a href="editcoupon.php?cid=<?php //echo $row_src->fields["coupon_id"];?>"><img src="img/pencil.png" border="0" /></a>
     &nbsp;&nbsp;&nbsp;-->
    <a href="manage_coupon.php?act=delete&cid=<?php echo $row_src->fields["coupon_id"];?>"><img src="img/cross.png" border="0" /></a>
   
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="7" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="7" align="center" height="25"><b>No Coupon Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>