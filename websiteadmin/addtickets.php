<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

$msgval="";

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Create")
{
		 
	$ins_emp = "insert into tbl_ticket_details set				
				cust_id			= '".$_REQUEST["cid"]."',
				plan_id			= '".$_REQUEST["selplan"]."',
				device_id 		= '".$_REQUEST["seldevice"]."',
				issue_id 		= '".$_REQUEST["seltechissue"]."',
				note_details	= '".$_REQUEST["notedet"]."',
				added_date		= '".date("Y-m-d")."',
				assign_to		= '".$_REQUEST["selemp"]."',				
				addedby			= '".$_SESSION['uniqID']."'";
				
			/*echo "<pre>";
			print_r($ins_emp);    	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		//url_redirect("addplan.php?add=1&cid=".$_REQUEST["cid"]);
		$msgval="Ticket has been added successfully.";
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	            
	 
	var selplan=trim(document.frmcrm.selplan.value);
	var seldevice=trim(document.frmcrm.seldevice.value);
	var seltechissue=trim(document.frmcrm.seltechissue.value);
	var selemp=trim(document.frmcrm.selemp.value);
	var notedet=trim(document.frmcrm.notedet.value);
	//var residntstate=trim(document.frmcrm.residntstate.value);
	//var callsource=trim(document.frmcrm.callsource.value);
	
	if(selplan=="")
	{
		alert('Please select plan.');
		document.frmcrm.selplan.focus();
		return false;
	}
	else if(seldevice=="")
	{
		alert('Please select device.');
		document.frmcrm.seldevice.focus();
		return false;
	}
	else if(seltechissue=="")
	{
		alert('Please select issue.');
		document.frmcrm.seltechissue.focus();
		return false;
	}
	else if(selemp=="")
	{
		alert('Please select employee.');
		document.frmcrm.selemp.focus();
		return false;
	}
	else if(notedet=="")
	{
		alert('Action plan should not be blank.');
		document.frmcrm.notedet.focus();
		return false;
	}
	else
	{
		
	}
	/*else if(callsource=="")
	{
		alert('Please select Call Source.');
		document.frmcrm.callsource.focus();
		return false;
	}*/
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker2").datepicker({minDate: -500, maxDate: +500});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -500, maxDate: +500});
	});	

//======= Ticket details ==========
function viewticket(incid)
{ 
	imgview=window.open("ticketview.php?incid="+incid,"Details",'height=500,width=850,resizable=1,scrollbars=1,screenX=200,screenY=100, menubar=1,fullscreen=1');
	
	if(window.focus)
	{
		imgview.focus();
	}
}

</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Add New Ticket</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <?php if($_REQUEST["add"]=="1"){?>
 <tr bgcolor="#D2EACC">
 <td colspan="2" align="center" height="25"><b>Plan has been added successfully.</b></td>
 </tr>
 <?php }?>
 <?php if($_REQUEST["edit"]=="2"){?>
 <tr bgcolor="#D2EACC">
 <td colspan="2" align="center" height="25"><b>Ticket has been updated successfully.</b></td>
 </tr>
 <?php }?>
 <?php if(!empty($msgval)){?>
 <tr bgcolor="#D2EACC">
 <td colspan="2" align="center" height="25"><b><?php echo $msgval;?></b></td>
 </tr>
 <?php }?>
 
 <tr>
    <td valign="top" width="50%">
    <fieldset style="border:1px solid #CCC; padding-left:10px;height:310px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Add Ticket</legend>
    <table border="0" width="95%" style="background-image:none; border:none;" cellpadding="5" cellspacing="0">
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Select Plan : </b></label></td>
    <td width="70%">
    <select name="selplan" id="selplan" class="widsel" onchange="seldevicelist(this.value);">
    <option value="">Select plan</option>
	<?php
	$sqlnx = "select * from  tbl_plan_details where cust_id ='".$_REQUEST["cid"]."' ";
	$resnx = $db->Execute($sqlnx);
	$totalnx  = $resnx->RecordCount();
	if($totalnx>0)
	{
	while (!$resnx->EOF) {	
	?>
    
    <option value="<?php echo $resnx->fields["plan_id"];?>"><?php echo $resnx->fields["plan_no"];?></option>
    <?php 
	$resnx->MoveNext();
	}
	}?>
    </select>  
    </td>
    </tr> 
    <tr>
    <td align="right"><label><b>Select Device : </b></label></td>
    <td>
    <div id="deviceval">
	<select name="seldevice" id="seldevice" class="widsel" >
	<option value="">- Select device -</option>
	</select>
    </div>
    </td>
    </tr>  
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Techanical Issue : </b></label></td>
    <td>
    <select name="seltechissue" id="seltechissue" class="widsel">
    <option value="">Select Techanical Issue</option>
	<?php
	$sqlnx = "select * from  tbl_techanical_issue where status ='0' ";
	$resnx = $db->Execute($sqlnx);
	$totalnx  = $resnx->RecordCount();
	if($totalnx>0)
	{
	while (!$resnx->EOF) {	
	?>
    
    <option value="<?php echo $resnx->fields["tech_id"];?>"><?php echo $resnx->fields["tech_issue"];?></option>
    <?php 
	$resnx->MoveNext();
	}
	}?>
    </select>  
    </td>
    </tr>   
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Assign To : </b></label></td>
    <td>
    <select name="selemp" id="selemp" class="widsel">
    <option value="">Select Employee</option>
	<?php
	$sqlnx = "select * from  tbl_adminuser where status  ='0' ";
	$resnx = $db->Execute($sqlnx);
	$totalnx  = $resnx->RecordCount();
	if($totalnx>0)
	{
	while (!$resnx->EOF) {	
	?>
    
    <option value="<?php echo $resnx->fields["id"];?>"><?php echo $resnx->fields["name"];?></option>
    <?php 
	$resnx->MoveNext();
	}
	}?>
    </select>  
    </td>
    </tr>    
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Action Plan : </b></label></td>
    <td>
    <textarea name="notedet" id="notedet" rows="5" cols="40" class="mini"></textarea>
    </td>
    </tr>   
    <tr>
    <td>&nbsp;</td>
    <td><input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Create" /></td>
    </tr>
    <tr>
    <td colspan="2" height="5"></td>
    </tr>
    </table>
    </fieldset>
    
    </td>
    <td valign="top" width="50%">
<?php
$sqlapp = "select * from tbl_newcall_details where new_id='".$_REQUEST["cid"]."' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();
?>
     <fieldset style="border:1px solid #CCC; padding-left:10px; padding-bottom:10px; height:310px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Account Details</legend>
        <table border="0" width="95%" style="background-image:none; border:none;" cellpadding="3" cellspacing="0">
    
    <tr>
    <td align="right" width="30%"><label><b>Account No. : </b></label></td>
    <td width="70%"><?php echo $resapp->fields["account_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Name : </b></label></td>
    <td><?php echo $resapp->fields["title"]." ".$resapp->fields["fname"]." ".$resapp->fields["lname"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Email : </b></label></td>
    <td><?php echo $resapp->fields["email_id"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Home No. : </b></label></td>
    <td><?php echo $resapp->fields["home_no"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Cell No. : </b></label></td>
    <td><?php echo $resapp->fields["cell_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Call Source : </b></label></td>
    <td><?php echo $resapp->fields["call_source"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Country : </b></label></td>
    <td><?php 
if($resapp->fields["selcountry"]=="1")
{
	echo "United State";
}
else
{
	echo "Canada";
}
?></td>
    </tr>
    <tr>
    <td align="right"><label><b>State : </b></label></td>
    <td><?php echo showcuststate($resapp->fields["selstate"]);?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Zip Code : </b></label></td>
    <td><?php echo $resapp->fields["zipcode"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>City : </b></label></td>
    <td><?php echo $resapp->fields["city_name"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Time Zone: </b></label></td>
    <td><?php echo $resapp->fields["time_zone"];?>  
    </td>
    </tr>
    </table>
    </fieldset>
</td>
  </tr>
  <tr>
  <tr>
  <td colspan="2">
    <div id="devhis">
    <?php
    $sqlpl = "select * from tbl_ticket_details where device_id ='".$_REQUEST["nids"]."' ";
$respl = $db->Execute($sqlpl);
$totalpl  = $respl->RecordCount();	
?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#CCCCCC">
<td colspan="8"><b>Device History Deails</b></td>
</tr>
<tr bgcolor="#999999">
<th height="25" width="10%" align="center">Ticket Id</th>
<th width="12%">Created Date</th>
<th width="34%">Issue</th>
<th width="14%" align="center">Assign to</th>
<th width="10%" align="center">Status</th>
<th width="12%" align="center">PC Name</th>
<th width="8%" align="center">Action</th>
</tr>
<?php
if($totalpl>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$respl->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td align="center"><?php echo $respl->fields["note_id"];?></td>
<td align="center"><?php echo date("d-m-Y",strtotime($respl->fields["added_date"]));?></td>
<td><?php echo $respl->fields["note_details"];?></td>
<td align="center"><?php echo show_empname($respl->fields["assign_to"]);?></td>
<td align="center">
<?php 
if($respl->fields["status"]=='0')
{
?>
<span style="color:#F00;"><b>Open</b></span>
<?php
}
else
{
?>
<span style="color:#030;"><b>Closed</b></span>
<?php	
}
?>
</td>
<td align="center"><?php echo device_pcname($respl->fields["device_id"]);?></td>
<td align="center">
<a href="javascript:void(0);" onclick="viewticket('<?php echo $respl->fields["note_id"];?>');"><img src="img/information.png" border="0" /></a>
&nbsp;&nbsp;&nbsp;

<a href="edittickets.php?cid=<?php echo $respl->fields["cust_id"];?>&tid=<?php echo $respl->fields["note_id"];?>"><img src="img/pencil.png" border="0" /></a></td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$respl->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="7" align="center" height="25"><b>No Device Details found.</b></td>
</tr>
<?php }?>
</table>
    </div>	
  </td>
  </tr>  
 </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>