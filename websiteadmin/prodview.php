<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();




$sqlapp = "select * from  tbl_subcategory where subcat_id='".$_REQUEST["incid"]."' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	     
	 
	var saluname=trim(document.frmcrm.saluname.value);
	var firstname=trim(document.frmcrm.firstname.value);
	var contactno=trim(document.frmcrm.contactno.value);
	var selcountry=trim(document.frmcrm.selcountry.value);
	var residntstate=trim(document.frmcrm.residntstate.value);
	var callsource=trim(document.frmcrm.callsource.value);
	
	if(saluname=="")
	{
		alert('Please select Salutation.');
		document.frmcrm.saluname.focus();
		return false;
	}
	else if(firstname=="")
	{
		alert('First name should not be blank.');
		document.frmcrm.firstname.focus();
		return false;
	}
	else if(contactno=="")
	{
		alert('Home No should not be blank.');
		document.frmcrm.contactno.focus();
		return false;
	}
	else if(selcountry=="")
	{
		alert('Please select country.');
		document.frmcrm.selcountry.focus();
		return false;
	}
	else if(residntstate=="")
	{
		alert('Please select state.');
		document.frmcrm.residntstate.focus();
		return false;
	}
	else if(callsource=="")
	{
		alert('Please select Call Source.');
		document.frmcrm.callsource.focus();
		return false;
	}
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<table width="100%" cellpadding="6" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;"> 
<tr>
<td colspan="3" bgcolor="#CCCCCC"><b>Product Details</b></td>
</tr>
<tr>
<td width="29%"><label><b><!--<span class="requfield">*</span>-->Product Name </b></label></td>
<td width="1%"><b>:</b></td>
<td width="70%"><?php echo $resapp->fields["product_name"];?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Product Subcategory Name  </b></label></td>
<td><b>:</b></td>
<td><?php echo subcategoryname($resapp->fields["category_id"]);?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Product Category Name </b></label></td>
<td><b>:</b></td>
<td><?php echo maincategory($resapp->fields["maincat_id"]);?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Manufacturer Name </b></label></td>
<td><b>:</b></td>
<td><?php echo show_manufacture($resapp->fields["manufacture_name"]);?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Rate </b></label></td>
<td><b>:</b></td>
<td><?php echo $resapp->fields["rate"];?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Product Description </b></label></td>
<td><b>:</b></td>
<td><?php echo $resapp->fields["descp"];?></td>
</tr>
<tr>
<td><label><b><!--<span class="requfield">*</span>-->Small Image </b></label></td>
<td><b>:</b></td>
<td><label><b>Big Image</b></label></td>
</tr>
<tr>
<td>
<?php if(!empty($resapp->fields["image"])){?>
<img src="<?php echo "../product_photo/".$resapp->fields["image"];?>" border="0" width="100" height="100" />
<?php }?>
</td>
<td><b>:</b></td>
<td>
<?php if(!empty($resapp->fields["big_img"])){?>
<img src="<?php echo "../product_photo/".$resapp->fields["big_img"];?>" border="0" width="300" height="300" />
<?php }?>
</td>
</tr>
</table>
</body>
</html>