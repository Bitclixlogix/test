<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_testimonials where  id !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (testimonial_by like '%".$srcval."%' or  testimonial_content like '%".$srcval."%') ";
}


$sqlsrc .= " order by created_date desc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_testimonials where  id !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (testimonial_by like '%".$srcval."%' or  testimonial_content like '%".$srcval."%') ";	
}

$sqltot .= " order by created_date desc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="3" align="right"><a href="adtestimonial.php">Add New Testimonial</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
<th width="65%" align="left">Testimonial</th>
<th width="20%" align="center">By</th>
<th width="15%" align="center">Action</th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
	<td align="left"><?php echo $row_src->fields["testimonial_content"];?></td>
    <td align="center"><?php echo $row_src->fields["testimonial_by"];?></td>    
    <td align="center"><a href="edittest.php?unid=<?php echo $row_src->fields["id"];?>"><img src="img/pencil.png" border="0" /></a>
    &nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delfun('<?php echo $row_src->fields["id"];?>')"><img src="img/cross.png" border="0" /></a>	
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="3" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="3" align="center" height="25"><b>No Testimonial Details found.</b></td>
</tr>
<?php }?>

</table>