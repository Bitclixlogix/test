<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_order_confirm_details where  conf_ordid !='' and status='0' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (order_no like '%".$srcval."%') ";
}


$sqlsrc .= " order by order_no desc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_order_confirm_details where conf_ordid !='' and status='0' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (order_no  like '%".$srcval."%') ";	
}

$sqltot .= " order by order_no desc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<!--<tr>
<td colspan="7" align="right"><a href="addcategory.php">Add New Category</a>&nbsp;&nbsp;</td>
</tr>-->
<tr bgcolor="#999999">
<th width="8%">Order No.</th>
<th width="15%">Name</th>
<th width="27%">Email</th>
<th width="11%" align="center">Mobile No.</th>
<th width="11%" align="center">Date</th>
<th width="8%" align="center">Amount</th>
<th width="15%" align="center">Status</th>
<th width="5%" align="center">Action</th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {

$rt_qry = "select * from tbl_register_user where user_unid='".$row_src->fields["user_unid"]."' ";
$resrt = $db->Execute($rt_qry);

?>
<tr <?php echo $bgcol;?>>
    <td align="left"><a href="orderdetails.php?cid=<?php echo $row_src->fields["order_no"];?>"><?php echo $row_src->fields["order_no"];?></a></td>
    <td align="left"><?php echo $resrt->fields["first_name"];?></td>
    <td align="left"><?php echo $resrt->fields["usr_email"];?></td>
    <td align="center"><?php echo $resrt->fields["contact_no"];?></td>
    <td align="center"><?php echo $row_src->fields["order_date"];?></td>
    <td align="center"><?php echo $row_src->fields["total_amount"];?></td>
    <td align="center">
    <?php if($row_src->fields["status"]=='0'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Order Confirm/Not Delivered</span>
    <?php } else if($row_src->fields["status"]=='2'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Suspended</span>
    <?php } else if($row_src->fields["status"]=='3'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Intransit/Dispatched</span>    
	<?php } else {?>
    <span style="color:#030; font-weight:bold; text-align:center;">Delivered</span>
    <?php }?>
    </td>
    <td align="center">
    <a href="editorder.php?cid=<?php echo $row_src->fields["conf_ordid"];?>"><img src="img/pencil.png" border="0" /></a>
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="6" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="6" align="center" height="25"><b>No Order Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>