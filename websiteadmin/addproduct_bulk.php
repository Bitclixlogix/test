<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
	 
	 //======== College photo =======================
	if($_FILES["usrphoto"]["size"]>0 && $_FILES["usrphoto"]["name"] !=" ") 
	{
		//=========== to upload the file =============		
		$photofile_name="";
		$uploaddir="../product_photo/";
		$uploadfrom = $_FILES["usrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["usrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $photofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$photofile_name)){
			  $photofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$photofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$photofile_name=" ";
	}
	
	
	//======== College photo =======================
	/*if($_FILES["bigusrphoto"]["size"]>0 && $_FILES["bigusrphoto"]["name"] !=" ") 
	{
		
		$bigphotofile_name="";
		$uploaddir="../product_photo/";
		$uploadfrom = $_FILES["bigusrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["bigusrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $bigphotofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$bigphotofile_name)){
			  $bigphotofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$bigphotofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$bigphotofile_name=" ";
	}*/
	 
	       
	$ins_emp = "insert into tbl_subcategory_bulk set
				maincat_id 		= '".$_REQUEST["selcategory"]."',
				category_id 	= '".$_REQUEST["selsubcat"]."',
				product_code 	= '".$_REQUEST["prdcode"]."',
				product_name 	= '".$_REQUEST["prdname"]."',
				product_color 	= '".$_REQUEST["prdcolor"]."',				
				previous_rate 	= '".$_REQUEST["prdmrp"]."',
				quantity 		= '".$_REQUEST["prdqty"]."',				
				manufacture_name= '".$_REQUEST["selmanu"]."',
				rate 			= '".$_REQUEST["prdrate"]."',
				descp 			= '".addslashes($_REQUEST["prddet"])."',
				manufacture_warranty = '".$_REQUEST["manuwarranty"]."',
				replace_warranty 	= '".$_REQUEST["replwarranty"]."',
				publishedby 	= '".$_REQUEST["pubby"]."',
				image			= '".$photofile_name."',				
				is_featured		= '0',
				subcat_status	= '0'";
				   
			/*echo "<pre>";
			print_r($ins_emp);   	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		url_redirect("mng_bulk_products.php?add=1");
	}
}

require '../fckeditor/fckeditor.php';
$oFCKeditor = new FCKeditor('prddet') ;
$oFCKeditor->BasePath	= '../fckeditor/' ;
$oFCKeditor->Width = '620px';
$oFCKeditor->Height = '500px';
$oFCKeditor->Value		="";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	     // prdname  
	          
	//var selcategory=trim(document.frmcrm.selcategory.value);
	//var selsubcat=trim(document.frmcrm.selsubcat.value)
	//var selmanu=trim(document.frmcrm.selmanu.value);
	var prdcode=trim(document.frmcrm.prdcode.value);
	var prdname=trim(document.frmcrm.prdname.value);
	var prdrate=trim(document.frmcrm.prdrate.value);
	var usrphoto=trim(document.frmcrm.usrphoto.value);
	//var bigusrphoto=trim(document.frmcrm.bigusrphoto.value);
	//var username=trim(document.frmcrm.username.value);
	//var newpass=trim(document.frmcrm.newpass.value);
	//var confpass=trim(document.frmcrm.confpass.value);
	
	/*if(selcategory=="")
	{
		alert('Please select category.');
		document.frmcrm.selcategory.focus();
		return false;
	}
	else if(selsubcat=="")
	{
		alert('Please select subcategory.');
		document.frmcrm.selsubcat.focus();
		return false;
	}
	else*/
	
	if(prdcode=="")
	{
		alert('Product code should not be blank.');
		document.frmcrm.prdcode.focus();
		return false;
	}
	else if(prdname=="")
	{
		alert('Product Name should not be blank .');
		document.frmcrm.prdname.focus();
		return false;
	}
	else if(prdrate=="")
	{
		alert('Product rate should not be blank .');
		document.frmcrm.prdrate.focus();
		return false;
	}
	else if(isNaN(prdrate))
	{
		alert('Product rate should be number only.');
		document.frmcrm.prdrate.focus();
		return false;
	}
	else if(usrphoto=="")
	{
		alert('Please select small photo .');
		document.frmcrm.usrphoto.focus();
		return false;
	}
	else if(usrphoto!=="")
	{
		var ss;
		ss=1 + usrphoto.lastIndexOf(".");
		var ext =usrphoto.substr(ss);
		ext=ext.toLowerCase();
		if (!(ext == "gif" || ext == "jpeg"|| ext == "jpg"))
		{		
			alert('You can upload only doc or pdf file.');
			document.frmcrm.usrphoto.value="";
			document.frmcrm.usrphoto.focus();
			return false;
		}
	}
	/*else if(bigusrphoto=="")
	{
		alert('Please select Big photo .');
		document.frmcrm.bigusrphoto.focus();
		return false;
	}
	else if(bigusrphoto!=="")
	{
		var ss;
		ss=1 + bigusrphoto.lastIndexOf(".");
		var ext =bigusrphoto.substr(ss);
		ext=ext.toLowerCase();
		if (!(ext == "gif" || ext == "jpeg"|| ext == "jpg"))
		{		
			alert('You can upload only doc or pdf file.');
			document.frmcrm.bigusrphoto.value="";
			document.frmcrm.bigusrphoto.focus();
			return false;
		}
	}*/
	
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Add New Bulk Product</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td valign="top">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Product Details</legend>
    <table border="0" width="95%" style="background-image:none; border:none;">
    <tr style="display:none;">
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Select Category : </b></label></td>
    <td width="70%">
    <?php
        $sql_ct = "select * from  tbl_category_main where status='0' order by cat_name";
        $resct = $db->Execute($sql_ct);
        $total_ct  = $resct->RecordCount();
        ?>
        <select name="selcategory" id="selcategory" class="widsel" onchange="fillsubcat(this.value);">
        <option value="">- Select Category -</option>
        <?php 
        if($total_ct>0){
        while (!$resct->EOF) {		
        ?>
        <option value="<?php echo $resct->fields["catid"];?>"><?php echo $resct->fields["cat_name"];?></option>
        <?php
        $resct->MoveNext();
        }
        }?>
        </select>
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Select Subcategory : </b></label></td>
    <td>
    <div id="selscat">
    <select name="selsubcat" id="selsubcat" class="widsel">
    <option value="">- Select Subcategory -</option>
    </select>
    </div>
    </td>
    </tr> 
    <tr style="display:none;">
    <td align="right"><label><b><!--<span class="requfield">*</span>-->Select Manufacture : </b></label></td>
    <td>
    <?php
        $sql_ct = "select * from  tbl_manufacture_details where status='0' order by manufacture_name";
        $resct = $db->Execute($sql_ct);
        $total_ct  = $resct->RecordCount();
        ?>
        <select name="selmanu" id="selmanu" class="widsel">
        <option value="">- Select Manufacture -</option>
        <?php 
        if($total_ct>0){
        while (!$resct->EOF) {		
        ?>
        <option value="<?php echo $resct->fields["manu_id"];?>"><?php echo $resct->fields["manufacture_name"];?></option>
        <?php
        $resct->MoveNext();
        }
        }?>
        </select>
    </td>
    </tr>
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Product Code : </b></label></td>
    <td width="70%"><input type="text"  name="prdcode" id="prdcode"  value="" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Product Name : </b></label></td>
    <td width="70%"><input type="text"  name="prdname" id="prdname"  value="" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b><!--<span class="requfield">*</span>-->Available Color : </b></label></td>
    <td width="70%"><input type="text"  name="prdcolor" id="prdcolor"  value="" class="widtxt" /></td>
    </tr>
     <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>MRP Rate : </b></label></td>
    <td><input type="text"  name="prdmrp" id="prdmrp"  value="" class="widtxt" /></td>
    </tr>
     <tr>
    <td align="right"><label><b><span class="requfield">*</span>Sale Rate : </b></label></td>
    <td><input type="text"  name="prdrate" id="prdrate"  value="" class="widtxt" /></td>
    </tr>
     <tr style="display:none;">
    <td align="right"><label><b><span class="requfield">*</span>Quantity : </b></label></td>
    <td><input type="text"  name="prdqty" id="prdqty"  value="" class="widtxt" /></td>
    </tr>
     <tr>
    <td align="right" valign="top"><label><b><!--<span class="requfield">*</span>-->Product Description : </b></label></td>
    <td><!--<textarea name="prddet" id="prddet" rows="5" cols="40" class="mini"></textarea>-->
    <?php echo $oFCKeditor->Create();?>
    </td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Manufacture Warranty : </b></label></td>
    <td width="70%"><input type="text"  name="manuwarranty" id="manuwarranty"  value="" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Replacement Warranty : </b></label></td>
    <td width="70%"><input type="text"  name="replwarranty" id="replwarranty"  value="" class="widtxt" /></td>
    </tr>
    <tr style="display:none;">
    <td align="right" width="30%"><label><b>Published By : </b></label></td>
    <td width="70%"><input type="text"  name="pubby" id="pubby"  value="" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Small Image (212x212) : </b></label></td>
    <td><input type="file" name="usrphoto" id="usrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>
   <!-- <tr>
    <td align="right"><label><b><span class="requfield">*</span>Big Image (300x350) : </b></label></td>
    <td><input type="file" name="bigusrphoto" id="bigusrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>-->
    <tr>
    <td align="right">&nbsp;</td>
    <td>
    <input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" />
    </td>
    </tr>  
    </table>
    </fieldset>
    </td>
  </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>