<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
	    
	  //======== College photo =======================
	if($_FILES["usrphoto"]["size"]>0 && $_FILES["usrphoto"]["name"] !=" ") 
	{
		//=========== to upload the file =============		
		$photofile_name="";
		$uploaddir="../brand_logo/";
		$uploadfrom = $_FILES["usrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["usrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $photofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$photofile_name)){
			  $photofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$photofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$photofile_name=" ";
	}
	 
	$ins_emp = "insert into tbl_manufacture_details set
				manufacture_name	= '".$_REQUEST["name"]."',
				brand_logo			= '".$photofile_name."',
				status		= '0'";
				
			/*echo "<pre>";
			print_r($ins_emp);   	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		url_redirect("manage_manu.php?add=1");
	}
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	     
	  
	var name=trim(document.frmcrm.name.value);
	var usrphoto=trim(document.frmcrm.usrphoto.value);
	//var username=trim(document.frmcrm.username.value);
	//var newpass=trim(document.frmcrm.newpass.value);
	//var confpass=trim(document.frmcrm.confpass.value);
	
	
	if(name=="")
	{
		alert('Manufacturer Name should not be blank .');
		document.frmcrm.name.focus();
		return false;
	}
	else if(usrphoto=="")
	{
		alert('Please select small photo .');
		document.frmcrm.usrphoto.focus();
		return false;
	}
	else if(usrphoto!=="")
	{
		var ss;
		ss=1 + usrphoto.lastIndexOf(".");
		var ext =usrphoto.substr(ss);
		ext=ext.toLowerCase();
		if (!(ext == "gif" || ext == "jpeg"|| ext == "jpg"))
		{		
			alert('You can upload only doc or pdf file.');
			document.frmcrm.usrphoto.value="";
			document.frmcrm.usrphoto.focus();
			return false;
		}
	}
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Add New Brands</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td valign="top">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">New Brands</legend>
    <table border="0" width="95%" style="background-image:none; border:none;">
    <tr>
    <td align="right" width="30%"><label><b><span class="requfield">*</span>Brands Name : </b></label></td>
    <td width="70%"><input type="text"  name="name" id="name"  value="" class="widtxt" /></td>
    </tr>
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>Brand Logo (any x 155) : </b></label></td>
    <td><input type="file" name="usrphoto" id="usrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>
    <tr>
    <td align="right">&nbsp;</td>
    <td>
    <input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" />
    </td>
    </tr>  
    </table>
    </fieldset>
    </td>
  </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>