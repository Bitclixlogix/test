<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
	 
	 //======== College photo =======================
	if($_FILES["usrphoto"]["size"]>0 && $_FILES["usrphoto"]["name"] !=" ") 
	{
		//=========== to upload the file =============		
		$photofile_name="";
		$uploaddir="../slider_images/";
		$uploadfrom = $_FILES["usrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["usrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $photofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$photofile_name)){
			  $photofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$photofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$photofile_name=$_REQUEST["oldimg"];
	}
	       
	$ins_emp = "update tbl_shop_category set	
				ban_url	= '".$_REQUEST["name"]."',	
				banner_size	= '".$_REQUEST["title"]."',		
				banner_image	= '".$photofile_name."' where ban_id='".$_REQUEST["cid"]."'";
				
			/*echo "<pre>";
			print_r($ins_emp);    	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if(ins_result)
	{
		url_redirect("shop-by-category.php?edit=2");
	}
}

$sqlapp = "select * from tbl_shop_category where ban_id='".$_REQUEST["cid"]."' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	
	var title=trim(document.frmcrm.title.value);
	var name=trim(document.frmcrm.name.value);
	var usrphoto=trim(document.frmcrm.usrphoto.value);
	
	if(title=="")
	{
		alert('Title should not be blank .');
		document.frmcrm.title.focus();
		return false;
	}
	else if(name=="")
	{
		alert('URL should not be blank .');
		document.frmcrm.name.focus();
		return false;
	}
	else if(usrphoto!=="")
	{
		var ss;
		ss=1 + usrphoto.lastIndexOf(".");
		var ext =usrphoto.substr(ss);
		ext=ext.toLowerCase();
		if (!(ext == "gif" || ext == "jpeg"|| ext == "jpg"))
		{		
			alert('You can upload only gif or jpeg file.');
			document.frmcrm.usrphoto.value="";
			document.frmcrm.usrphoto.focus();
			return false;
		}
	}
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Shop By Category Update</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
<input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />
<input type="hidden" name="oldimg" id="oldimg" value="<?php echo $resapp->fields["banner_image"];?>" />
<table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td valign="top">
    <fieldset style="border:1px solid #CCC; padding-left:10px;">
    <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Shop By Category Details</legend>
    <table border="0" width="95%" style="background-image:none; border:none;">
     <tr>
    <td align="right"><label><b><span class="requfield">*</span>Title : </b></label></td>
    <td><input type="text"  name="title" id="title"  value="<?php echo $resapp->fields["banner_size"];?>" class="widtxt" style="width:600px;" /></td>
    </tr> 
    <tr>
    <td align="right"><label><b><span class="requfield">*</span>URL : </b></label></td>
    <td><input type="text"  name="name" id="name"  value="<?php echo $resapp->fields["ban_url"];?>" class="widtxt" style="width:600px;" /></td>
    </tr> 
     <tr>
    <td align="right"><label><b><span class="requfield">*</span> Image (480x453) : </b></label></td>
    <td><input type="file" name="usrphoto" id="usrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>
   <tr><td colspan="2" height="10"></td></tr>
    <tr>
    <td align="right">&nbsp;</td>
    <td>
    <input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" />
    </td>
    </tr>  
    <tr><td colspan="2" height="20"></td></tr>
    <tr>
    <td align="right"><b>Banner</b></td>
     <td align="left">
	<?php if(!empty($resapp->fields["banner_image"])){?>
    <img src="<?php echo "../slider_images/".$resapp->fields["banner_image"];?>" border="0"  />
    <?php }?>
    </td>
    </tr>
    </table>
    </fieldset>
    </td>
  </tr>
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>