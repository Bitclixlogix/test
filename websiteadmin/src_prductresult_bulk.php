<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_subcategory_bulk where  subcat_id  !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (product_code = '".$srcval."' or product_name  like '%".$srcval."%' or descp  like '%".$srcval."%') ";
}


$sqlsrc .= " order by product_name asc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_subcategory_bulk where  subcat_id  !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (product_code = '".$srcval."' or product_name  like '%".$srcval."%' or descp  like '%".$srcval."%') ";	
}

$sqltot .= " order by product_name asc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="5" align="right"><a href="addproduct_bulk.php">Add New Bulk Product</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
<th width="44%" align="left">Product Name</th>
<!--<th width="20%" align="left">Subcategory Name</th>
<th width="18%" align="left">Category Name</th>-->
<th width="15%"><center>Rate</center></th>
<th width="15%"><center>Image</center></th>
<th width="10%"><center>Status</center></th>
<!--<th width="15%" align="center">Order</th>-->
<th width="16%"><center>Action</center></th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
    <td align="left"><?php echo $row_src->fields["product_name"];?></td>
    <!--<td align="left"><?php //echo subcategoryname($row_src->fields["category_id"]);?></td>
    <td align="left"><?php //echo maincategory($row_src->fields["maincat_id"]);?></td>-->
    <td align="center"><?php echo $row_src->fields["rate"];?></td>
    <td align="center">
     <?php 
	if(!empty($row_src->fields["image"]) && file_exists("../product_photo/".$row_src->fields["image"])) { 
			
			$imgdet=getimagesize("../product_photo/".$row_src->fields["image"]);
		
	?>
	<a href="javascript:view_large_image(<?php echo $row_src->fields["subcat_id"];?>,<?php echo $imgdet[0];?>,<?php echo $imgdet[1];?>)">
    <img src="thumb_posm.php?thumbfile=<?php echo "../product_photo/".$row_src->fields["image"];?>" border="0" /></a>
	<?php  }?>
    </td>
    <td align="center">
    <?php if($row_src->fields["subcat_status"]=='1'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Inactive</span>
    <?php } else {?>
    <span style="color:#030; font-weight:bold; text-align:center;">Active</span>
    <?php }?>
    </td>
    <td align="center">
   <!-- <a href="javascript:void(0);" onClick="viewprd('<?php //echo $row_src->fields["subcat_id"];?>');"><img src="img/information.png" border="0" /></a>
    &nbsp;&nbsp;-->
    <a href="editproduct_bulk.php?cid=<?php echo $row_src->fields["subcat_id"];?>"><img src="img/pencil.png" border="0" /></a>
     &nbsp;&nbsp;
    <a href="mng_bulk_products.php?act=delete&cid=<?php echo $row_src->fields["subcat_id"];?>"><img src="img/cross.png" border="0" /></a>
    <!--&nbsp;&nbsp;
    <a href="addmoreimg.php?cid=<?php //echo $row_src->fields["subcat_id"];?>">+More</a>-->
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="5" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="5" align="center" height="25"><b>No Product Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>