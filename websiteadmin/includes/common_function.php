<?php
$dayarr=array('1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10,'11'=>11,'12'=>12,'13'=>13,'14'=>14,'15'=>15,'16'=>16,'17'=>17,'18'=>18,'19'=>19,'20'=>20,'21'=>21,'22'=>22,'23'=>23,'24'=>24,'25'=>25,'26'=>26,'27'=>27,'28'=>28,'29'=>29,'30'=>30,'31'=>31);

$monarr=array('01'=>"JAN",'02'=>"FEB",'03'=>"MAR",'04'=>"APR",'05'=>"MAY",'06'=>"JUN",'07'=>"JUL",'08'=>"AUG",'09'=>"SEP",'10'=>"OCT",'11'=>"NOV",'12'=>"DEC");

$qtyrate=array('0'=>'Rejected','1'=>'Poor','2'=>'Average','3'=>'Good','4'=>'Great','5'=>'Outstanding');


function watermarkImage ($SourceFile, $WaterMarkText, $DestinationFile) {
   
   ini_set('memory_limit', '1024M');
	set_time_limit ( 0 );

   list($width, $height) = getimagesize($SourceFile);
   $image_p = imagecreatetruecolor($width, $height);
   $image = imagecreatefromjpeg($SourceFile);
   //$image =$SourceFile;
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
   $black = imagecolorallocate($image_p, 241, 230, 56);
   $font = 'arial.ttf';
   $font_size = 15;
   imagettftext($image_p, $font_size, 0, 10, 60, $black, $font, $WaterMarkText);
   if ($DestinationFile<>'') {
      imagejpeg ($image_p, $DestinationFile, 100);
   } else {
      header('Content-Type: image/jpeg');
      imagejpeg($image_p, null, 100);
   };
   imagedestroy($image);
   imagedestroy($image_p);
};


//============= Page url redirection function ==============
function urlredirect($url)
{
	if (headers_sent()) {
		echo "<script>document.location.href='$url';</script>\n";
	} else {
		@ob_end_clean(); // clear output buffer
		header( 'HTTP/1.1 301 Moved Permanently' );
		header( "Location: ". $url );
	}
}
//======== function check login user =======
function check_userlogin_menu()
{
	global $db;
	
	if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id']))
	{
		url_redirect('index.php');
	}
	else
	{
		//echo "<pre>";
		//print_r($_SERVER[]);
		//echo substr($_SERVER["SCRIPT_NAME"],11);
		//die;
		
		if($_SERVER["SERVER_NAME"]=="localhost")
				$urlname=substr($_SERVER["SCRIPT_NAME"],11);
		else
				$urlname=substr($_SERVER["SCRIPT_NAME"],1);
				
		//========= runtime check=========
		$sqlusr = "select * from member where uniqID ='".$_SESSION['uniqID']."'";		
		$rowusr = $db->Execute($sqlusr);
		
		$sqlchk = "select * from tbl_menudetails where accessby like '%".$rowusr->fields['access_id']."%'";
		$sqlchk .=" and menu_url='".$urlname."' and  status ='0'";
		$rowcheck = $db->Execute($sqlchk);
		
		$totalchk= $rowcheck->RecordCount();
		
		if($totalchk>0)
		{			
		}
		else
		{
			if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))
			{
				session_unregister($_SESSION['user_id']);
				session_unregister($_SESSION['access_id']);
				session_unregister($_SESSION['username']);
				session_unregister($_SESSION['name']);
				session_unregister($_SESSION['uniqID']);	
				session_unregister($_SESSION['status']);
				
				session_destroy();
			}
			url_redirect('index.php');
		}
		
	}
}

//======== function check login user =======
function check_admlogin()
{
	global $db;
	
	if(!isset($_SESSION['uniqID']) && empty($_SESSION['uniqID']))
	{
		url_redirectindex('index.php');
	}	
}


//============ Country  Name ==========
function category_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category where catid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cat_name"];
}

function device_pcname($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_device_detail where device_id  ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["computer_name"];
}


function statelist_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_state_list where state_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["state_name"];
}

function login_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_adminuser where id  ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["name"];
}
/*
//============ Country  Name ==========
function country_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_country where country_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["country_name"];
}

//============ Country  Name ==========
function state_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_state where state_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["state_name"];
}

//============ Country  Name ==========
function city_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_city where city_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["city_name"];
}

*/






//============ Show star value ===========
function takearg($sval)
{
	if($sval>=0.5 && $sval<=1)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=1 && $sval<1.5)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=1.5 && $sval<2.0)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/ratinghalf.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=2.0 && $sval<2.5)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=2.5 && $sval<3)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/ratinghalf.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=3.0 && $sval<3.5)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=3.5 && $sval<4)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/ratinghalf.jpg" border="0" ><img src="images/grayrating.jpg" border="0" >';
	}
	else if($sval>=4.0 && $sval<4.5)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/ratinghalf.jpg" border="0" >';
	}
	else if($sval>=4.5 && $sval<=5)
	{
		echo '<img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" ><img src="images/rating.jpg" border="0" >';
	}
}

//*********************** Company Project Start *******************************************
function pmworkaverage($pmunid)
{
	global $db;
	
	$sql = "SELECT sum(totalscore/11) as totcol from tbl_student_work_feedback,tbl_projectwise_pm ";
	$sql.= " where project_id = posted_prj_id and pm_unid ='".$pmunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	$finalv=round($row->fields["totcol"]/$totrec,1);
	return $finalv;
}

function pmcompletedprj($pmunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_projectwise_pm,tbl_post_project";
	$sql.= " where posted_prj_id=post_prj_id and prj_status='4' and  pm_unid='".$pmunid."' ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function pmongoingprj($pmunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_projectwise_pm,tbl_post_project,tbl_bid_details";
	$sql.= " where posted_prj_id=post_prj_id and post_prj_id=project_id and assign_status='1' ";
	$sql.= "and prj_status!='4' and  pm_unid='".$pmunid."' group by post_prj_id  ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function upheldprj($pmunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_projectwise_pm,tbl_post_project,tbl_bid_details";
	$sql.= " where posted_prj_id=post_prj_id and post_prj_id=project_id and assign_status!='1' ";
	$sql.= "and prj_status!='4' and  pm_unid='".$pmunid."' group by post_prj_id  ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//********************* Company Project end ***********************************************

//***************** Company dash ********************************************
//===== Company Average Result ===========
function companyaverage($comunid)
{
	global $db;
	
	$sql = "SELECT sum(totalscore/11) as totcol from tbl_student_work_feedback,tbl_post_project ";
	$sql.= " where project_id = post_prj_id and company_createdby ='".$comunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	$finalv=round($row->fields["totcol"]/$totrec,1);
	return $finalv;
}

//========= get jobstatus project ===========
function companyjobreq($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_job where job_cmp_createdby='".$comunid."' and job_status!='1' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;	
}

//========= get jobstatus project ===========
function companyjobfullfil($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_job where job_cmp_createdby='".$comunid."' and job_status='1' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;	
}


//========== College Engaged=====
function compengagedcollege($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details,tbl_post_project";
	$sql.= " where project_id=post_prj_id and  company_createdby='".$comunid."' and assign_status='1' group by college_unid ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;	
}

//========== Student Engaged=====
function compengagedstu($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_project_wstudent,tbl_post_project";
	$sql.= " where project_id=post_prj_id and  company_createdby='".$comunid."'  group by student_unid ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;	
}


//======= Company College faviourite ========
function comfavcollege($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_company_favourites where company_unid='".$comunid."' group by college_unid  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}
 
//======= Company Student faviourite ========
function compfavstu($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_company_favourites where company_unid='".$comunid."' group by student_unid  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//======  Total number of Company PM =====
function comptotpm($comunid)
{
	global $db;
	
	$sql = "SELECT * from member where cmp_hr_pm_creator='".$comunid."' and access_id='8'  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//======  Total number of Company HR =====
function comptothr($comunid)
{
	global $db;
	
	$sql = "SELECT * from member where cmp_hr_pm_creator='".$comunid."' and access_id='9'  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//========== Underconsideration Project ================
function underconsiderationprj($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_project where company_createdby='".$comunid."' and prj_status='0'  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//========== Accepted Project ================
function acceptedprj($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_project where company_createdby='".$comunid."' and prj_status='0' and approved_by!=''  ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//========== Rejected Project ================
function rejectedprj($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_project where company_createdby='".$comunid."' and prj_status='5'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function compimptilldate($comunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_bid_details,tbl_post_project";
	$sql.= " where project_id=post_prj_id and prj_status='4' and  company_createdby='".$comunid."' ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function totbidtildate($comunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_bid_details,tbl_post_project";
	$sql.= " where project_id=post_prj_id and company_createdby='".$comunid."' ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//========= ongoing bid ===========
function ongoingbidcomp($comunid)
{
	global $db;

	//$sql = "SELECT * from tbl_bid_details where college_unid='".$colunid."' and assign_status!='1' ";
	$sql= "SELECT * from tbl_bid_details,tbl_post_project";
	$sql.= " where project_id=post_prj_id and company_createdby='".$comunid."' and assign_status!='1' and approved_by!='' ";
	
	$resch = $db->Execute($sql);
	$totrec= $resch->RecordCount();	
		
	return $totrec;	
}

//===== Student are getting benefited by Company ===========
function totbenefstucomp($comunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_project_wstudent,tbl_post_project ";
	$sql.= " where project_id=post_prj_id and company_createdby='".$comunid."' group by student_unid";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}




//**************************** Company Dash Board end ************************

//**************** Student Dash *********************************************
//===== Student Average Score ===========
function stuaverage($stuunid)
{
	global $db;
	
	$sql = "SELECT sum(totalscore/11) as totcol from tbl_student_work_feedback where student_unid ='".$stuunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	$finalv=round($row->fields["totcol"]/$totrec,1);
	return $finalv;
}

//===== Total procompleted ====
function totcompleted($stuunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_project_wstudent,tbl_post_project";
	$sql.= " where project_id=post_prj_id and prj_status='4' and  student_unid='".$stuunid."' ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//====== student completed particular category project ===========
function totcategorycompleted($stuunid,$catid)
{
	global $db;
	
	$sql= "SELECT * from tbl_project_wstudent,tbl_post_project";
	$sql.= " where project_id=post_prj_id and prj_status='4' and student_unid='".$stuunid."' and proj_categoryid='".$catid."'  ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}




//**********************Student Dash end ************************************


//*********************** College Dash Start *********************************
//====== Total bid of a project =========
function totbidofcollege($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details where college_unid='".$colunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//====== Total bid of a project =========
function totAssignedofcollege($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details where college_unid='".$colunid."' and assign_status='1' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function totpendingofcollege($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details where college_unid='".$colunid."' and assign_status='0' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//====== Total college Implemented till this date =========
function collegeimptilldate($colunid)
{
	global $db;
	
	$sql= "SELECT * from tbl_bid_details,tbl_post_project";
	$sql.= " where project_id=post_prj_id and prj_status='4' and  college_unid='".$colunid."' ";
	
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//========= get my all project ===========
function myalllostproject($colunid)
{
	global $db;
	$prjid=0;
	
	$sql = "SELECT * from tbl_bid_details where college_unid='".$colunid."' and assign_status='0' ";
	$resch = $db->Execute($sql);
	$totrec= $resch->RecordCount();
	
	/*$inch_qry = "select * from tbl_projectwise_pm where pm_unid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	$totrec= $resch->RecordCount();*/
	if($totrec>0)
	{
		 while (!$resch->EOF) 
		 {
			 
			$sqlr = "SELECT * from tbl_bid_details where college_unid!='".$colunid."' and assign_status='1' ";
			$resr = $db->Execute($sqlr);
			$totrv= $resr->RecordCount();
			if($totrv>0)
			{
				$prjid++;
			}
			 
			 
			 $resch->MoveNext();
		 }	 
		
	}
	
	$totlos=$totrec-$prjid;
	
	return $totlos;
	
}

//===== Student are getting benefit ===========
function totbenefitedstu($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_project_wstudent where college_unid='".$colunid."' group by student_unid";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//===== Student are getting benefit ===========
function prjavailibility($colunid)
{
	global $db;
	
	$sql = "SELECT * from member where uniqID='".$colunid."' ";
	$row = $db->Execute($sql);
	
	
	return $row->fields["project_availibility"];;
}

//======= Show college average ==========
//===== Student are getting benefit ===========
function collegeaverage($colunid)
{
	global $db;
	
	$sql = "SELECT sum(totalscore/11) as totcol from tbl_student_work_feedback where college_unid ='".$colunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	$finalv=round($row->fields["totcol"]/$totrec,1);
	return $finalv;
}




//========= get jobstatus project ===========
function jobfullfilopen($colunid)
{
	global $db;
	$req=0;
	$close=0;
	
	$sql = "SELECT * from tbl_post_job ";
	$resch = $db->Execute($sql);
	$totrec= $resch->RecordCount();
	
	if($totrec>0)
	{
		 while (!$resch->EOF) 
		 {
			 $jobarr=explode(",",$resch->fields["job_available_for"]);
			 
			if(in_array($colunid,$jobarr) and $resch->fields["job_status"]=="1")
			{
				$close++;
			}
			
			if(in_array($colunid,$jobarr))
			{
				$req++;
			}
			
			
			 
			 $resch->MoveNext();
		 }	 
		
	}
	
	$totlos=$req."#".$close;
	
	return $totlos;
	
}

//*********************** College Dash End ***************************


function totresponse($jobid)
{
	global $db;
	
	$sql = "SELECT * from tbl_studentjob_announcement where job_id='".$jobid."'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//============ Caste  Name ==========
function totgroupstudents($reqid,$colunid)
{
	global $db;
	$inch_qry = "select * from tbl_colgroup_student_details where group_id ='".$reqid."' and college_unid='".$colunid."' ";
	$resch = $db->Execute($inch_qry);
	$totrec= $resch->RecordCount();
	return $totrec;
}


//============ Caste  Name ==========
function allmemsh_det($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch;
}

//============ Caste  Name ==========
function postproject_details($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_post_project where post_prj_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch;
}

//======== Position for =============
function shpositionfor($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_post_job where post_job_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["position_for"];
}


//======== job company name =============
function shcompname($reqid)
{
	global $db;
	$inch_qry = "select member.* from tbl_post_job,member where job_cmp_createdby=uniqID and post_job_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["company"];
}

//======== Show project category ===============
function shprojectcat($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_project_category where cat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["prj_cat"];
}

//======= Project Manager Name ========
function shprojectmanager($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_projectwise_pm where posted_prj_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["pm_unid"];
}

//======== Brief About ===============
function briefabout($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["stu_brief_about"];
}

//======== Brief About ===============
function trainig_tpoic($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_training_details where train_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["train_title"];
}


//============ Caste  Name ==========
function student_name($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["first"]." ".$resch->fields["last"];
}

//============ Caste  Name ==========
function boardsh_name($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_board where board_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["board_name"];
}

//============ Caste  Name ==========
function shgroup_name($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_group_details where grp_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["group_name"];
}

//============ Caste  Name ==========
function stream_name($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_stream where stream_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["stream_name"];
}

function project_name($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_post_project where post_prj_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["project_name"];
}

function project_categoryid($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_post_project where post_prj_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["proj_categoryid"];
}

//============ Caste  Name ==========
function shcompany_name($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["company"];
}




//============ employee unid return company uniqid ==========
function retcompanyunid($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cmp_hr_pm_creator"];
}

//============ student unid return college uniqid ==========
function retcollegeunid($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["stucollege_approver"];
}

//============ Caste  Name ==========
function shcitybyid($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["city"];
}

//============ Caste  Name ==========
function membertype($reqid)
{
	global $db;
	$inch_qry = "select * from member where uniqID ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["access_id"];
}

//============ Module Name ==========
function module_shname($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_project_time_management where ptime_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["module_name"];
}


//============ get unique key ==========
function auto_uniq($reqid)
{
	global $db;
	$inch_qry = "select * from member where username ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["uniqID"];
}

//========= Total no of student ==========

function totworkingstu($colunid,$prjid)
{
	global $db;
	
	$sql = "SELECT * from tbl_project_wstudent where college_unid='".$colunid."' and project_id='".$prjid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

function totworkingstuproject($prjid)
{
	global $db;
	
	$sql = "SELECT * from tbl_project_wstudent where project_id='".$prjid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}

//========= get my all project ===========
function myallproject($reqid)
{
	global $db;
	$prjid='';
	$inch_qry = "select * from tbl_projectwise_pm where pm_unid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	$totrec= $resch->RecordCount();
	if($totrec>0)
	{
		 while (!$resch->EOF) 
		 {
			 $prjid.="'".$resch->fields["posted_prj_id"]."',";
			 
			 $resch->MoveNext();
		 }
		 
		 return $prjid;
	}
	else
	{
		return $prjid;
	}
}


//============ Country  Name ==========
function country_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_country where country_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["country_name"];
}

//============ Country  Name ==========
function state_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_state where state_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["state_name"];
}

//============ Country  Name ==========
function city_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_city where city_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["city_name"];
}

//============ User type  Name ==========
function usertype($reqid)
{
	global $db;
	$inch_qry = "select * from tbl_usertype where usrtype_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["usertype_name"];
}

//======= Total posted project by a company =========
function totposted_project($compunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_project where company_createdby='".$compunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//======= Total posted project by a company =========
function totcollege_project($compunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details where college_unid='".$compunid."' and assign_status='1' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//======= Total posted project by a company =========
function assign_college($prjid)
{
	global $db;
	$inch_qry = "select * from tbl_bid_details where project_id ='".$prjid."' and assign_status='1' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["college_unid"];
}

//======= Total posted project by a company =========
function jobpositionfor($prjid)
{
	global $db;
	$inch_qry = "select * from tbl_post_job where post_job_id ='".$prjid."'";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["position_for"];
}


//======= Total posted project by a company =========
function college_university($coluid)
{
	global $db;
	$inch_qry = "select * from tbl_college_ext where college_unid ='".$coluid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["affiliated_university"];
}

//======= Total posted job by a company =========
function totposted_job($compunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_post_job where job_cmp_createdby='".$compunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//======= Total bid till date by a company =========
function totbidtilldate($compunid)
{
	global $db;
	
	$sql = "select tbl_post_project.* from tbl_post_project,tbl_bid_details where post_prj_id=project_id and company_createdby='".$compunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


function colbidtilldate($compunid)
{
	global $db;
	
	$sql = "select tbl_post_project.* from tbl_post_project,tbl_bid_details where post_prj_id=project_id and college_unid='".$compunid."' ";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}



//========= show total not approved college ===============
function showtotcollege()
{
	global $db;
	
	$sql = "SELECT * from member where approved_status='1' and 	access_id='4'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}


//========= show total not approved college ===============
function studentcollege($stuid)
{
	global $db;
	
	$sql = "SELECT * from member where uniqID='".$stuid."' and 	access_id='3'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $row->fields["stucollege_approver"];
}


//========= show total not approved college ===============
function collegeabout($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_college_ext where college_unid='".$colunid."'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $row->fields["college_contact_det"];
}

//========= show total not approved college ===============
function industry_type($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_industry_type where ind_id='".$colunid."'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $row->fields["industry_name"];
}


function collegestream($colunid)
{
	global $db;
	
	$sql = "SELECT * from tbl_college_ext where college_unid='".$colunid."'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $row->fields["courses_offered"];
}

//========= show total not approved college ===============
function showtotprojectbid($proid)
{
	global $db;
	
	$sql = "SELECT * from tbl_bid_details where project_id='".$proid."'";
	$row = $db->Execute($sql);
	$totrec= $row->RecordCount();
	
	return $totrec;
}
?>