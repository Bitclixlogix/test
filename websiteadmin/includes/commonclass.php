<?php
class commondevcls
{
	//========== Show pagination details ============
	function showpagination($targetpage,$total,$page)
	{
		$pg1=1;
		$pg2=2;
		
		// How many adjacent pages should be shown on each side?
		$adjacents = 2;
		
		global $limit;
		
		//echo $total."==".$limit;
		//die;
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		
		
		$pagination = "";
		/*echo $lastpage;
		die;*/
		if($lastpage > 1)
		{	
			 $pagination .= "<div class=\"pagination\">";
			//previous button
			if ($page > 1) 
			{
				$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$prev.'\');">&laquo; Previous</a>';
				
			}
			else
			{
				//$pagination.= "<span class=\"disabled\">First</span>";	
				$pagination.= "<span class=\"disabled\">&laquo; Previous</span>";	
			}
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						 $pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
					}
					$pagination.= "...";
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lpm1.'\');">'.$lpm1.'</a>';
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lastpage.'\');">'.$lastpage.'</a>';	
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg1.'\');">1</a>';
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg2.'\');">2</a>';
					$pagination.= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
					}
					$pagination.= "...";
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lpm1.'\');">'.$lpm1.'</a>';
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lastpage.'\');">'.$lastpage.'</a>';
					
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg1.'\');">1</a>';
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg2.'\');">2</a>';
					$pagination.= "...";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';
							
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
			{
				$next_page = $page+1;
				
				$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$next_page.'\');">Next &raquo;</a>';
				
			}
			else
			{
				$pagination.= "<span class=\"disabled\">Next &raquo;</span>";
				//$pagination.= "<span class=\"disabled\">Last </span>";
			}
			$pagination.= "</div>\n";		
		}
		/*echo $pagination;
		die;*/
		
		return $pagination;
	}
	
	//========== show select project prefix =============
	function pro_prefix($pid)
	{
		global $db;
		$sql = "SELECT * from tbl_projectdetails where project_id='".$pid."'";
		$row = $db->Execute($sql);		
		return $row->fields['prefix'];
	}	
	
	//========== show selected project name =============
	function pro_title($pid)
	{
		global $db;
		$sql = "SELECT * from tbl_projectdetails where project_id='".$pid."'";
		$row = $db->Execute($sql);		
		return $row->fields['project_name'];
	}	
	
	//========== show user type of all member =============
	function pro_directory($pid)
	{
		global $db;
		$sql = "SELECT * from tbl_projectdetails where project_id='".$pid."'";
		$row = $db->Execute($sql);		
		return $row->fields['directory'];
	}
	
	//========= show user name only ===================
	function show_onlyname($pid)
	{
		global $db;
		$sql = "SELECT first,middle,last,username from member where uniqID='".$pid."'";
		$row = $db->Execute($sql);	
		
		return ucwords(strtolower($row->fields['first']));
	}
	
	//========== show user type of all member =============
	function show_usrtyp($pid)
	{
		global $db;
		$sql = "SELECT name from usertype where id='".$pid."'";
		$row = $db->Execute($sql);		
		return $row->fields['name'];
	}

	//================= Only merchandiser list ==========================================
	function getmyonlymerchandizers (&$stores, $uniqID, $prjid) 
	{
		global $db;
		global $merchandiser_usertype;
		
		$ownsql="SELECT member FROM tbl_orgstructure ";
		$ownsql.="WHERE  owner = '".$uniqID."' and  project_id ='".$prjid."' ";
		
		$row = $db->Execute($ownsql);
		
		while (!$row->EOF) 
		{	
			
			$chown=" SELECT access_id,first,last,tbl_userproject.user_type as user_type  FROM member, tbl_userproject ";
			$chown.=" WHERE tbl_userproject.useruniqID =member.uniqID and ";
			$chown.=" tbl_userproject.useruniqID = '".$row->fields["member"]."' and project_id ='".$prjid."'  LIMIT 1 ";
			
			$rowacc = $db->Execute($chown);
			
			if (in_array($rowacc->fields["user_type"],$merchandiser_usertype))
					$stores[] = $row->fields["member"];		
			else 
				$this->getmyonlymerchandizers($stores,$row->fields["member"]);
			
			$row->MoveNext();
		}
	}

	//============= Get my all store =================
	function getmyallstores(&$stores, $uniqID) 
	{
		global $db;
		
		$row = $db->Execute("SELECT member FROM orgstructure WHERE  owner = '".$uniqID."'");
		
		while (!$row->EOF) 
		{	
			//echo $row->fields["member"];
			
			$rowacc = $db->Execute("SELECT access_id,first,last FROM member WHERE uniqID = '".$row->fields["member"]."' LIMIT 1 ");
			
			if ($rowacc->fields["access_id"]==6)
				$stores .= "'".$row->fields["member"]."',";
			else if ($rowacc->fields["access_id"]==7)
			{
				//$stores .= "'".$row->fields["member"]."',";
				$this->getmyallstores($stores,$row->fields["member"]);
			}
			else if($rowacc->fields["access_id"]==2 || $rowacc->fields["access_id"]==8)
			{
				//$stores .= "'".$row->fields["member"]."',";
				$this->getmyallstores($stores,$row->fields["member"]);
			}		
			else 
				$this->getmyallstores($stores,$row->fields["member"]);
			
			$row->MoveNext();
		}
	}
	
	// ======== Show my all level member ======================
	function getmyallmem(&$stores, $uniqID) 
	{
		global $db;
		
		$row = $db->Execute("SELECT member FROM orgstructure WHERE  owner = '".$uniqID."'");
		
		while (!$row->EOF) 
		{	
			$rowacc = $db->Execute("SELECT access_id,first,last FROM member WHERE uniqID = '".$row->fields["member"]."' LIMIT 1 ");
			
			if ($rowacc->fields["access_id"]==7)
			{
				$stores .= "'".$row->fields["member"]."',";
				//getmyall($stores,$row->fields["member"]);
			}
			else if($rowacc->fields["access_id"]==2 || $rowacc->fields["access_id"]==8)
			{
				$stores .= "'".$row->fields["member"]."',";
				$this->getmyallmem($stores,$row->fields["member"]);
			}		
			else 
				$this->getmyallmem($stores,$row->fields["member"]);
			
			$row->MoveNext();
		}
	}
	
	//========== show only name of the user 
	function show_parentname($pid)
	{
		global $db;
		$sql = "SELECT * from geography where Id ='".$pid."'";
		$row = $db->Execute($sql);	
		
		return $row->fields['gName'];
	}
	
	//======== Show owner of the parent ===========
	function show_parent($pid)
	{
		global $db;
		$sql = "SELECT owner from orgstructure where member='".$pid."'";
		$row = $db->Execute($sql);
		
		return $row->fields['owner'];
	}
	
	//============= take id and return UNIQID ====================
	function show_unid($pid)
	{
		global $db;
		$sql = "SELECT uniqID from member where id='".$pid."'";
		$row_unid = $db->Execute($sql);
		
		return $row_unid->fields["uniqID"];
	}
	
	//============project name =========
	function show_projname($pid)
	{
		global $db;
		$sql = "SELECT * from  tbl_projectdetails where project_id='".$pid."'";
		$row_unid = $db->Execute($sql);
		
		return $row_unid->fields["project_name"];
	}
	
	//========= Take member uniqID and return user type =========
	//============project name =========
	function show_usertype($pid)
	{
		global $db;
		$sql = "SELECT name FROM usertype,member WHERE usertype.id=member.access_id and member.uniqID='".$pid."'";
		$row_unid = $db->Execute($sql);
		
		return $row_unid->fields["name"];
	}
	
	
	
}
?>
