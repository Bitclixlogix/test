// JavaScript Document
//======== check valid email ===========
function isEmail (value)
{
	var theStr=value;
	var atIndex = theStr.indexOf('@');
	var dotIndex = theStr.indexOf('.', atIndex);
	var flag = true;
	theSub = theStr.substring(0, dotIndex+1)

	if ((atIndex < 1)||(atIndex != theStr.lastIndexOf('@'))||(dotIndex < atIndex + 2)||(theStr.length <= theSub.length))
	{
		//alert("Invalid E-mail ID");
		flag = false;
	}
	else
	{ 
		flag = true;
	}
	return(flag);
}

//========= to check special character =========
function ischeckspecial(strfield)
{
	var iChars = "%^&*()+=-[]\\\';,./{}|\":<>? ";
	//var usrval="document.regfrm."+strfield+".value";
	
	//alert(usrval);
	for (var i = 0; i < strfield.length; i++)
	{
		if (iChars.indexOf(strfield.charAt(i)) != -1)
		{		
			return false;
		}
		
	}
	return true;
}

//======== To trim the user input value =======
function trim(inputString)
{
	inputString=inputString.replace(/^\s+/g,"");
	inputString=inputString.replace(/\s+$/g,"");
	return inputString;
}

//========= only integer value allow====
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
