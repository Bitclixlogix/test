<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_adminuser where id !='' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and (name ='".$srcval."' or username  like '%".$srcval."%') ";
}


$sqlsrc .= " order by name asc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_adminuser where id !='' ";

if(!empty($srcval))
{	 
	$sqltot.= " and (name ='".$srcval."' or username  like '%".$srcval."%') ";	
}

$sqltot .= " order by name asc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="7" align="right"><a href="adduser.php">Add New Customer</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
    <th height="25" width="20%" align="left">Username</th>
    <th width="30%">Name</th>
    <th width="15%" align="center">User Type</th>
    <th width="15%" align="center">Status</th>
    <th width="20%" align="center">Action</th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
    <td align="left"><?php echo $row_src->fields["username"];?></td>
    <td><?php echo $row_src->fields["name"];?></td>
    <td align="center">
    <?php if($row_src->fields["user_type"]=='1'){?>
    <span style="font-weight:bold; text-align:center;">Admin</span>
    <?php } else {?>
    <span style="font-weight:bold; text-align:center;">Support</span>
    <?php }?>
    </td>
    <td align="center">
    <?php if($row_src->fields["status"]=='1'){?>
    <span style="color:#F00;font-weight:bold; text-align:center;">Inactive</span>
    <?php } else {?>
    <span style="color:#030; font-weight:bold; text-align:center;">Active</span>
    <?php }?>
    </td>
    <td align="center">
    <a href="edituser.php?cid=<?php echo $row_src->fields["id"];?>"><img src="img/pencil.png" border="0" /></a> 
    &nbsp;&nbsp;
    <a href="edituserpass.php?cid=<?php echo $row_src->fields["id"];?>">Reset Password</a>    
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="5" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="5" align="center" height="25"><b>No User Details found.</b></td>
</tr>
<?php }?>
<!--<tr bgcolor="#EFEFEF">
    <td>Trident</td>
    <td>Internet
         Explorer 5.0</td>
    <td>Win 95+</td>
    <td class="center">5</td>
    <td class="center">C</td>
    <td class="center"> 4</td>
    <td class="center"><a href="#">+Note</a>&nbsp;&nbsp;<a href="#">+Device</a>&nbsp;&nbsp;
    <a href="#">+Ticket</a></td>
</tr>-->
</table>