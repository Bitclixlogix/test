<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function validform()
{
	                 
	var fromdt=document.getElementById('datepicker').value;
	var todt=document.getElementById('datepicker1').value;
	var srcval=document.getElementById('txtsrc').value;	
	search_custajax(fromdt,todt,srcval,'1');
}

function getpage(fromdt,todt,srcval,page)
{
	search_custajax(fromdt,todt,srcval,page);

}



//==== Validate form filled by user ================== 
function checkusrval()
{
	//alert('qwqe');
	var usrname=trim(document.frmadmlog.usrname.value);
	var usrpass=trim(document.frmadmlog.usrpass.value);
	
	if(usrname=="")
	{
		alert("User name should not be blank.");
		document.frmadmlog.usrname.focus();
		return false;
	}	
	else if(usrpass=="")
	{
		alert("Password should not be blank.");
		document.frmadmlog.usrpass.focus();
		return false;
	}
	
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -30, maxDate: +30});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -30, maxDate: +30});
	});	


function viewemployee(incid)
{ 
	imgview=window.open("customerview.php?incid="+incid,"Details",'height=500,width=850,resizable=1,scrollbars=1,screenX=200,screenY=100, menubar=1,fullscreen=1');
	
	if(window.focus)
	{
		imgview.focus();
	}
}
</script>          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
        <div class="box round first">
            <!--<h2>Customer Management</h2>-->
            <?php
            $sqlapp = "select * from tbl_newcall_details where new_id='".$_REQUEST["cid"]."' ";
        $resapp = $db->Execute($sqlapp);
        $totalapp  = $resapp->RecordCount();
        ?>
        <div style="text-align:center;"><h3 style="margin:0px;"><?php echo $resapp->fields["account_no"];?> - <?php echo $resapp->fields["title"]." ".$resapp->fields["fname"]." ".$resapp->fields["lname"];?></h3></div>
            <?php if($_REQUEST["add"]=="5"){?>
            <div class="message success"><p><b>Note has been added successfully.</b></p></div>
            <?php }?>
        <form name="frmcrm" id="frmcrm" action="" method="post" onsubmit="return validateinfo();" enctype="multipart/form-data">
        <input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />

        <table width="100%" cellpadding="5" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
        <tr>   
        <td width="50%" valign="top">
        <?php
		$sqlpl = "select * from tbl_plan_details where cust_id='".$_REQUEST["cid"]."' ";
		$respl = $db->Execute($sqlpl);
		$totalpl  = $respl->RecordCount();
		?>
        <fieldset style="border:1px solid #CCC; padding-left:10px; height:370px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Plan Details</legend>
        <table width="100%" cellpadding="4" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
<!--<tr bgcolor="#999999">
<th height="25" width="15%" align="center">Plan Name</th>
<th width="65%">Devices</th>
<th width="20%" align="center">Added By</th>
</tr>-->
<?php
if($totalpl>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$respl->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td width="30%"><b><?php echo $respl->fields["plan_no"];?></b></td>
<td align="center" width="25%"><?php echo $respl->fields["tenure_start_date"];?></td>
<td align="center" width="25%"><?php echo $respl->fields["tenure_end_date"];?></td>
<td align="center" width="20%"><?php echo $respl->fields["amount"];?></td>
</tr>
<tr <?php echo $bgcol;?>>
<td align="left"><?php echo $respl->fields["devices"];?>&nbsp;Devices</td>
<td align="center"><?php echo $respl->fields["tenure"];?> Years</td>
<td align="right" colspan="2">
<a href="#" style="color:#039;"><b>Renew</b></a>&nbsp;&nbsp;
<a href="#" style="color:#039;"><b>Upgrade</b></a>
</td>
</tr>
<tr><td colspan="4"><hr color="#FF0000" style="margin:0px; padding:0px;" /></td></tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$respl->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="4" align="center" height="25"><b>No Plan Details found.</b></td>
</tr>
<?php }?>
<tr>
<td colspan="2"><a href="" style="color:#039;">Read More...</a> </td>
<td colspan="2" align="right" height="25"><a href="addnoted.php?cid=<?php echo $_REQUEST["cid"];?>" style="color:#039;"><b>Add New Plan</b></a></td>
</tr>
</table>        
        
        
        </fieldset>
        </td>
        <td width="50%" valign="top">
		        <fieldset style="border:1px solid #CCC; padding-left:10px; height:370px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Customer Details</legend>
        <table border="0" width="95%" style="background-image:none; border:none;" cellpadding="3" cellspacing="0">
    
    <tr>
    <td align="right" width="30%"><label><b>Account No. : </b></label></td>
    <td width="70%"><?php echo $resapp->fields["account_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Name : </b></label></td>
    <td><?php echo $resapp->fields["title"]." ".$resapp->fields["fname"]." ".$resapp->fields["lname"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Email : </b></label></td>
    <td><?php echo $resapp->fields["email_id"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Home No. : </b></label></td>
    <td><?php echo $resapp->fields["home_no"];?></td> 
    </tr>
    <tr>
    <td align="right"><label><b>Cell No. : </b></label></td>
    <td><?php echo $resapp->fields["cell_no"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Call Source : </b></label></td>
    <td><?php echo $resapp->fields["call_source"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Country : </b></label></td>
    <td><?php 
if($resapp->fields["selcountry"]=="1")
{
	echo "United State";
}
else
{
	echo "Canada";
}
?></td>
    </tr>
    <tr>
    <td align="right"><label><b>State : </b></label></td>
    <td><?php echo showcuststate($resapp->fields["selstate"]);?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Zip Code : </b></label></td>
    <td><?php echo $resapp->fields["zipcode"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>City : </b></label></td>
    <td><?php echo $resapp->fields["city_name"];?></td>
    </tr>
    <tr>
    <td align="right"><label><b>Time Zone: </b></label></td>
    <td><?php echo $resapp->fields["time_zone"];?>  
    </td>
    </tr>
    <tr>
    <td align="right"><label><b>Added By: </b></label></td>
    <td><?php echo show_empname($resapp->fields["addedby"]);?>  
    </td>
    </tr>
    <tr>
    <td align="right"><label><b>Timestamp: </b></label></td>
    <td><?php echo date("d-m-Y H:i:s",strtotime($resapp->fields["created_date"]));?>  
    </td>
    </tr>
   <tr>
<td colspan="2" align="right"><a href="addnoted.php?cid=<?php echo $_REQUEST["cid"];?>" style="color:#039;"><b>Edit Customer</b></a></td>
</tr>
    </table>
        </fieldset>
        </td>
		</tr>
        <tr>   
        <td valign="top">
        <?php
		$sqldec = "select * from tbl_device_detail where cust_id='".$_REQUEST["cid"]."' ";
		$resdec = $db->Execute($sqldec);
		$totaldec  = $resdec->RecordCount();
		?>
        <fieldset style="border:1px solid #CCC; padding-left:10px; height:235px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Device Details</legend>
        <table width="100%" cellpadding="4" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#999999">
<th height="25" width="25%" align="center">Plan Name</th>
<th width="20%">Make</th>
<th width="25%">Computer Name</th>
<th width="15%">OS Name</th>
<th width="15%" align="center">Plan Expiry </th>
</tr>
<?php
if($totaldec>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$resdec->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td width="25%"><b><?php echo show_planname($resdec->fields["plan_id"]);?></b></td>
<td align="center" width="20%"><?php echo $resdec->fields["make"];?></td>
<td align="center" width="25%"><?php echo $resdec->fields["computer_name"];?></td>
<td align="center" width="15%"><?php echo $resdec->fields["os_name"];?></td>
<td align="center" width="15%"><?php echo show_planexpiry($resdec->fields["plan_id"]);?></td>
</tr>

<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$resdec->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="5" align="center" height="25"><b>No Device Details found.</b></td>
</tr>
<?php }?>
<tr>
<td colspan="2"><a href="" style="color:#039;">Read More...</a> </td>
<td colspan="3" align="right" height="25"><a href="addnoted.php?cid=<?php echo $_REQUEST["cid"];?>" style="color:#039;"><b>Add New Device</b></a></td>
</tr>
</table>        

        </fieldset>
        </td>
        <td valign="top">
        <?php
		$sqltic = "select * from tbl_ticket_details where cust_id='".$_REQUEST["cid"]."' limit 5 ";
		$restic = $db->Execute($sqltic);
		$totaltic  = $restic->RecordCount();
		?>
        <fieldset style="border:1px solid #CCC; padding-left:10px; height:235px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Ticket Details</legend>
        <table width="100%" cellpadding="4" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#999999">
<th height="25" width="13%" align="center">Ticket No.</th>
<th width="20%">PV Name</th>
<th width="20%">Date</th>
<th width="35%">Issue</th>
<th width="12%"><center>Status</center></th>
</tr>
<?php
if($totaltic>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$restic->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td align="center"><?php echo $restic->fields["note_id"];?></td>
<td align="center"><?php echo device_pcname($restic->fields["device_id"]);?></td>
<td align="center"><?php echo date("d-m-Y",strtotime($restic->fields["added_date"]));?></td>
<td><?php echo techissue($restic->fields["issue_id"]);?></td>
<td align="center">
<?php 
if($restic->fields["status"]=="0")
{
	?>
    <span style="color:#F00;"><b>Open</b></span>
	<?php
    //echo "Open";
}
else
{
	?>
    <span style="color:#060;"><b>Closed</b></span>
    <?php 
    //echo "Closed";
}
?></td>
</tr>

<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$restic->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="5" align="center" height="25"><b>No Ticket Details found.</b></td>
</tr>
<?php }?>
<tr>
<td colspan="2"><a href="" style="color:#039;">Read More...</a> </td>
<td colspan="3" align="right" height="25"><a href="addnoted.php?cid=<?php echo $_REQUEST["cid"];?>" style="color:#039;"><b>Add New Ticket</b></a></td>
</tr>
</table>        

        </fieldset>        
        </td>
		</tr>
        <tr>   
        <td colspan="2">
        <?php
		$sqlnote = "select * from tbl_note_details where cust_id='".$_REQUEST["cid"]."' ";
		$resnote = $db->Execute($sqlnote);
		$totalnote  = $resnote->RecordCount();
		?>
        <fieldset style="border:1px solid #CCC; padding-left:10px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Note Details</legend>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr bgcolor="#999999">
<th height="25" width="15%" align="center">Date</th>
<th width="65%">Note Details</th>
<th width="20%" align="center">Added By</th>
</tr>
<?php
if($totalnote>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$resnote->EOF) {


?>
<tr <?php echo $bgcol;?>>
<td align="center"><?php echo date("d-m-Y",strtotime($resnote->fields["added_date"]));?></td>
<td><?php echo $resnote->fields["note_details"];?></td>
<td align="center"><?php echo show_empname($resnote->fields["addedby"]);?></td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$resnote->MoveNext();
}

}
else
{
?>
<tr>
<td colspan="3" align="center" height="25"><b>No Note Details found.</b></td>
</tr>
<?php }?>
<tr>
<td colspan="3" align="right" height="25"><a href="addnoted.php?cid=<?php echo $_REQUEST["cid"];?>" style="color:#039;"><b>Add New Note</b></a></td>
</tr>
</table>        
        
        
        </fieldset>        
        </td>
		</tr>
        </table>
        </form>
         <!-- Table data end-->
        </div>
        </div>            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>

</body>
</html>