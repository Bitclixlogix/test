<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");
check_userlogin();

$msgval="";
if($_SERVER['REQUEST_METHOD']=="POST" && $_REQUEST["btnsubmit"]=="Save")
{
	 
	   //======== College photo =======================
	if($_FILES["usrphoto"]["size"]>0 && $_FILES["usrphoto"]["name"] !=" ") 
	{
		//=========== to upload the file =============		
		$photofile_name="";
		$uploaddir="../brand_logo/";
		$uploadfrom = $_FILES["usrphoto"]["tmp_name"];
		   
		if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES["usrphoto"]["name"], $matches)){
		  } 
			  $nr = 0;
			  $photofile_name = $matches[1] . '.' . $matches[2];
			  while(file_exists($uploaddir.$photofile_name)){
			  $photofile_name = $matches[1] . '~'. $nr++ .'.' . $matches[2];
		  }		
		$uploaddir = $uploaddir.$photofile_name; 
				
		move_uploaded_file($uploadfrom, $uploaddir);
	}
	else
	{
		$photofile_name=$_REQUEST["smimg"];
	}
	   
	 
	$ins_emp = "update tbl_deal_of_day set
				from_date 		= '".date("Y-m-d",strtotime($_REQUEST["startdt"]))."',
				to_date 		= '".date("Y-m-d",strtotime($_REQUEST["enddt"]))."',
				shoping_url 	= '".$_REQUEST["name"]."',
				prd_image 		= '".$photofile_name."',
				status			= '".$_REQUEST["catsta"]."',
				deal_details	= '".addslashes($_REQUEST["attdevice"])."' where day_id='1'";
				
			/*echo "<pre>";
			print_r($ins_emp);   	                 
			die;*/
			
		$ins_result = $db->Execute($ins_emp) or die(mysql_error());
		
	if($ins_result)
	{
		//url_redirect("serviceboard.php?add=2");
		$msgval="Deal of the day has been updated successfully.";
	}
}


$sqlapp = "select * from tbl_deal_of_day where day_id='1' ";
$resapp = $db->Execute($sqlapp);
$totalapp  = $resapp->RecordCount();

require '../fckeditor/fckeditor.php';
$oFCKeditor = new FCKeditor('attdevice') ;
$oFCKeditor->BasePath	= '../fckeditor/' ;
$oFCKeditor->Width = '620px';
$oFCKeditor->Height = '500px';
$oFCKeditor->Value	=stripslashes($resapp->fields["deal_details"]);




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo PROJECT_TITLE;?></title>
	<link rel="stylesheet" type="text/css" href="css/text.css" />
    <link rel="stylesheet" type="text/css" href="css/grid.css"  />
    <link rel="stylesheet" type="text/css" href="css/layout.css"/>
    <link rel="stylesheet" type="text/css" href="css/nav.css"  />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="includes/jquery.js" type="text/javascript"></script>
<script language="javascript" src="includes/classified_ajax.js" type="text/javascript"></script>    
<script src="includes/javascript_function.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
//==== Validate form filled by user ================== 
function validateinfo()
{
	     
	 
	var saluname=trim(document.frmcrm.saluname.value);
	var firstname=trim(document.frmcrm.firstname.value);
	var contactno=trim(document.frmcrm.contactno.value);
	var selcountry=trim(document.frmcrm.selcountry.value);
	var residntstate=trim(document.frmcrm.residntstate.value);
	var callsource=trim(document.frmcrm.callsource.value);
	
	if(saluname=="")
	{
		alert('Please select Salutation.');
		document.frmcrm.saluname.focus();
		return false;
	}
	else if(firstname=="")
	{
		alert('First name should not be blank.');
		document.frmcrm.firstname.focus();
		return false;
	}
	else if(contactno=="")
	{
		alert('Home No should not be blank.');
		document.frmcrm.contactno.focus();
		return false;
	}
	else if(selcountry=="")
	{
		alert('Please select country.');
		document.frmcrm.selcountry.focus();
		return false;
	}
	else if(residntstate=="")
	{
		alert('Please select state.');
		document.frmcrm.residntstate.focus();
		return false;
	}
	else if(callsource=="")
	{
		alert('Please select Call Source.');
		document.frmcrm.callsource.focus();
		return false;
	}
}
</script>
<!-- CAlendar Code here-->
<link type="text/css" href="calen/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="calen/jquery-1.3.2.js"></script>
<script type="text/javascript" src="calen/ui/ui.datepicker.js"></script>
<link type="text/css" href="calen/demos.css" rel="stylesheet" />
<script language="javascript" type="text/javascript">

$(function() {
			 
		$("#datepicker").datepicker({minDate: -10, maxDate: +60});
	});	

$(function() {
			 
		$("#datepicker1").datepicker({minDate: -10, maxDate: +60});
	});	
</script>
          
</head>

<body style="margin:0px; padding:0px;">
<div class="container_12">
        <div class="grid_12 header-repeat">
        <?php include("includes/service_top.php");?>
        </div>
        <div class="clear">
        </div>
        <?php //include("includes/topmenu.php");?>
        <div class="clear">
        </div>
        <div class="grid_2">
        <?php include("includes/leftmenu.php");?>    
        </div>
        <div class="grid_10">
            <div class="box round first">
                <h2>Manage Deal of Day</h2>
                <div class="block">
                <!-- Table data start-->
<form name="frmcrm" id="frmcrm" action="" method="post"  enctype="multipart/form-data">
<input type="hidden" name="cid" id="cid" value="<?php echo $_REQUEST["cid"];?>" />
<input type="hidden" name="smimg" id="smimg" value="<?php echo $resapp->fields["prd_image"];?>" />
<table width="100%" cellpadding="6" cellspacing="0" border="0" style="border:0px solid #999; border-collapse:collapse; font-size:12px;">
 <tr>
    <td>
        <fieldset style="border:1px solid #CCC; padding-left:10px; padding-bottom:10px;">
        <legend style="background-color:#CCC; color:#333; font-weight:bold; padding:6px;">Deal of Day Details</legend>
        <table border="0" width="95%" style="background-image:none; border:none;">
        <?php if(isset($msgval) && !empty($msgval)){?>
		<tr>
        <td height="25" colspan="3" align="center" style="color:#900;"><b><?php echo $msgval;?></b></td>
        </tr>  
		<?php }?>
        
        <tr>
        <td width="19%" valign="top"><label><b><span class="requfield">*</span>From date</b></label></td>
        <td width="1%" valign="top"><b>:</b></td>
        <td width="80%"><input type="text" name="startdt" class="dp-choose-date" id="datepicker" value="<?php echo date("m/d/Y",strtotime($resapp->fields["from_date"]));?>"  style="width:130px; background-color:#FFF;" ></td>
        </tr>
        <tr>
        <td width="19%" valign="top"><label><b><span class="requfield">*</span>To Date</b></label></td>
        <td width="1%" valign="top"><b>:</b></td>
        <td width="80%"><input type="text" name="enddt" class="dp-choose-date" id="datepicker1" value="<?php echo date("m/d/Y",strtotime($resapp->fields["to_date"]));?>"  style="width:130px; background-color:#FFF;" ></td>
        </tr>
        <tr>
        <td width="19%" valign="top"><label><b><span class="requfield">*</span>Shoping URL</b></label></td>
        <td width="1%" valign="top"><b>:</b></td>
        <td width="80%"><input type="text"  name="name" id="name"  value="<?php echo $resapp->fields["shoping_url"];?>" style="width:600px;" class="widtxt" /></td>
        </tr>
        <tr>
        <td width="19%" valign="top"><label><b><span class="requfield">*</span>Deal of Day Details</b></label></td>
        <td width="1%" valign="top"><b>:</b></td>
        <td width="80%"><?php echo $oFCKeditor->Create();?></td>
        </tr>
        <tr>
        <td align="right"><label><b><span class="requfield">*</span> Status : </b></label></td>
         <td width="1%" valign="top"><b>:</b></td>
        <td>
        <input type="radio" name="catsta" id="catsta" <?php if($resapp->fields["status"]=="0") { echo 'checked="checked"';}?> value="0" />Active&nbsp;
        <input type="radio" name="catsta" id="catsta" <?php if($resapp->fields["status"]=="1") { echo 'checked="checked"';}?> value="1" />Inactive&nbsp;
        </td>
        </tr>
        <tr>
    <td align="right"><label><b><span class="requfield">*</span>Image (650x650) : </b></label></td>
     <td width="1%" valign="top"><b>:</b></td>
    <td><input type="file" name="usrphoto" id="usrphoto" class="mini" onkeypress="return false"  /></td>
    </tr>
        <tr>
        <td colspan="2"></td>
		<td align="left">
    	<input class="submit-green" name="btnsubmit" id="btnsubmit" type="submit" value="Save" />    
       </td>
		</tr>
        </table>
        </fieldset>
        <br /><br />
    <?php if(!empty($resapp->fields["prd_image"])){?>
    <img src="<?php echo "../brand_logo/".$resapp->fields["prd_image"];?>" border="0" width="450" height="450" />
    <?php }?>
    </td>
  </tr>
 
</table>
</form>	               
                <!-- Table data end-->
                </div>
            </div>
            
        </div>
        
        
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <?php include("includes/footer.php");?>
</body>
</html>