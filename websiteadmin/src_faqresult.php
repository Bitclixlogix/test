<?php
session_start();
include('../configuration.php');
include("includes/php_devfunction.php");

check_userlogin();

$page = $_REQUEST['page'];
if($page) 
	$start = ($page - 1) * $limit; 			//first item to display on this page
else
	$start = 0;	


$srcval=$_REQUEST["srcval"];

$targetpage = "'".$srcval."',";

//====== Retrieve login user details ===============
$sqlsrc=" select * from  tbl_news_details where  news_id !='' and rec_type='2' ";

if(!empty($srcval))
{	 
	$sqlsrc.= " and news_title like '%".$srcval."'%";
}


$sqlsrc .= " order by news_title asc";

$sqlsrc .= " LIMIT $start,$limit";

$row_src = $db->Execute($sqlsrc);
$total_src  = $row_src->RecordCount();

//************************* Total record as per condition ********************************************
$sqltot=" select * from  tbl_news_details where  news_id !='' and rec_type='2' ";

if(!empty($srcval))
{	 
	$sqltot.= " and news_title like '%".$srcval."%'  ";	
}

$sqltot .= " order by news_title asc";

$rowrec = $db->Execute($sqltot);
$total  = $rowrec->RecordCount();
//======= How many number of page =========	

$pagination=showpagination($targetpage,$total,$page);

?>
<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #999; border-collapse:collapse; font-size:12px;">
<tr>
<td colspan="2" align="right"><a href="addnews_faqs.php">Add New Blog</a>&nbsp;&nbsp;</td>
</tr>
<tr bgcolor="#999999">
<th width="90%" align="left">Blog Title</th>

<!--<th width="15%" align="center">Order</th>-->
<th width="10%"><center>Action</center></th>
</tr>
<?php
if($total_src>0){
$bgcol='bgcolor="#FFFFFF"';
while (!$row_src->EOF) {


?>
<tr <?php echo $bgcol;?>>
    <td align="left"><?php echo $row_src->fields["news_title"];?></td>   
    <td align="center">    
    <a href="editnews-faqs.php?cid=<?php echo $row_src->fields["news_id"];?>"><img src="img/pencil.png" border="0" /></a>
     &nbsp;&nbsp;
    <a href="manage-faq.php?act=delete&cid=<?php echo $row_src->fields["news_id"];?>"><img src="img/cross.png" border="0" /></a>
   
    </td>
</tr>
<?php

if($bgcol=='bgcolor="#FFFFFF"') { $bgcol='bgcolor="#EFEFEF"';} else { $bgcol='bgcolor="#FFFFFF"';}

$row_src->MoveNext();
}
?>
<tr>
<td colspan="2" align="center"><?php echo $pagination; ?></td>
</tr>
<?php
}
else
{
?>
<tr>
<td colspan="2" align="center" height="25"><b>No Blog Details found.</b></td>
</tr>
<?php }?>
</table>