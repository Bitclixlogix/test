<?php
session_start();
include('configuration.php');
include("includes/common_function.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo PROJECT_TITLE;?></title>

<!-- Favicon -->
<link rel="icon" href="images/favicon.png">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="all" />

<!-- Font Awesome CSS -->
<script src="https://use.fontawesome.com/baa5d86801.js"></script>

<!-- Main Template CSS -->
<link rel="stylesheet" href="css/style.css" media="all" />
<link rel="stylesheet" href="css/color/default.css" media="all" id="colors" />

</head>

<body>

<!-- ========== Header Section Start ========== -->

<header>
 <?php include("includes/header.php");?>
</header>

<!-- ========== Header Section End ========== --> 

<!-- ========== Banner Section Start ========== -->

<div class="pagemain_banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h1 class="color-white font-50 font-weight-400 text-uppercase xs-text-center">Contact Us</h1>
      </div>
    </div>
  </div>
</div>

<!-- ========== Banner Section End ========== -->

<section class="contact_form">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <h2 class="font-32 color-dark-grey no_padding no_margin">Get in touch</h2>
        <p class="font-16 color-grey padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr />
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-sm-12 col-xs-12 margin_top_30">
        <form name="sentMessage" id="contactForm" novalidate="">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Your Name *" id="name" required="" data-validation-required-message="Please enter your name.">
                <p class="help-block text-danger"></p>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" placeholder="Your Email *" id="email" required="" data-validation-required-message="Please enter your email address.">
                <p class="help-block text-danger"></p>
              </div>
              <div class="form-group">
                <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required="" data-validation-required-message="Please enter your phone number.">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <textarea class="form-control" placeholder="Your Message *" id="message" required="" data-validation-required-message="Please enter a message."></textarea>
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 text-center">
              <div id="success"></div>
              <input type="submit" class="btn_primary" value="Send Message" />
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="box_effect border_effect margin_top_30 text-center"><i class="fa fa-globe font-32"></i>
          <h5 class="font-22 padding_tb_20 ">Our Offices</h5>
          <p>Lorem ipsum dolor sit amet,<br> consectetur adipiscing<br>
            consectetur adipiscing<br>
            Fax : +001-123- 456789 </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ========== Footer Section Start ========== -->

<footer>
<?php include("includes/footer.php");?>
</footer>

<!-- ========== Footer Section End ========== --> 

<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 

<!-- Counter JavaScript --> 
<script type="text/javascript" src="js/jquery.waypoints.min.js"></script> 
<script type="text/javascript" src="js/jquery.counterup.min.js"></script> 
<script type="text/javascript" src="js/main.js"></script>

</body>
</html>
