<?php
session_start();
include('configuration.php');
include("includes/common_function.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo PROJECT_TITLE;?></title>

<!-- Favicon -->
<link rel="icon" href="images/favicon.png">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="all" />

<!-- Font Awesome CSS -->
<script src="https://use.fontawesome.com/baa5d86801.js"></script>

<!-- Main Template CSS -->
<link rel="stylesheet" href="css/style.css" media="all" />
<link rel="stylesheet" href="css/color/default.css" media="all" id="colors" />
</head>

<body>

<!-- ========== Header Section Start ========== -->

<header>
<?php include("includes/header.php");?>
</header>

<!-- ========== Header Section End ========== --> 

<!-- ========== Banner Section Start ========== -->

<div class="main_banner text-left">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 xs-text-center">
        <h1 class="color-white font-50 font-weight-400 text-uppercase">Dedicated Server</h1>
        <h2 class="color-white font-32 padding_tb_40">Fast affordable Web hosting Services.</h2>
        <p class="color-white font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut</p>
        <a class="btn_primary margin_top_50 display-inline-block" href="dedicated-server.html">Get Started Now</a> </div>
      <div class="col-md-6  hidden-xs hidden-sm"><img class="home_img" src="images/home-cion.png" alt="" title="" /></div>
    </div>
  </div>
</div>
</div>

<!-- ========== Banner Section End ========== --> 

<!-- ========== Our Services Section Start ========== -->

<section class="our_services text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h2 class="font-32 color-dark-grey no_padding no_margin">Our Services</h2>
        <p class="font-16 color-grey padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr />
      </div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box_effect border_effect margin_top_30">
          <div class="font-40"><i class="fa fa-tachometer" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_top_20">Shared Hosting</h4>
          <p class="color-grey font-16 padding_tb_20">Lorem Ipsum is simply dummy text of the printing and industry has been.</p>
          <a class="btn_primary display-inline-block z-index-1" href="shared-hosting.html">Buy Now</a> </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box_effect border_effect margin_top_30">
          <div class="font-40"><i class="fa fa-delicious" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_top_20">Reseller Hosting</h4>
          <p class="color-grey font-16 padding_tb_20">Lorem Ipsum is simply dummy text of the printing and industry has been.</p>
          <a class="btn_primary display-inline-block z-index-1" href="reseller-hosting.html">Buy Now</a> </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box_effect border_effect margin_top_30">
          <div class="font-40"><i class="fa fa-jsfiddle" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_top_20">Dedicated Server</h4>
          <p class="color-grey font-16 padding_tb_20">Lorem Ipsum is simply dummy text of the printing and industry has been.</p>
          <a class="btn_primary display-inline-block z-index-1" href="dedicated-server.html">Buy Now</a> </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="box_effect border_effect margin_top_30">
          <div class="font-40"><i class="fa fa-tasks" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_top_20">VPS Hosting</h4>
          <p class="color-grey font-16 padding_tb_20">Lorem Ipsum is simply dummy text of the printing and industry has been.</p>
          <a class="btn_primary display-inline-block z-index-1" href="vps-hosting.html">Buy Now</a> </div>
      </div>
    </div>
  </div>
</section>

<section class="background_primary text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h2 class="font-32 color-white no_padding no_margin">Our Top Most Services</h2>
        <p class="font-16 color-white padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr class="white" />
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-window-restore" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Domains &amp; Emails</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-cloud-download" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Fast Load Time</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-ticket" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Money Back Guarantee</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-window-restore" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Domains &amp; Emails</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-cloud-download" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Fast Load Time</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="top_services padding_20 margin_top_30">
          <div class="font-40"><i class="fa fa-ticket" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Money Back Guarantee</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ========== Top Services Section End ========== --> 

<!-- ========== Powerful Industries Section Start ========== -->

<section class="powerful_industries">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <h2 class="font-32 color-dark-grey no_padding no_margin">Our Powerful Industries</h2>
        <p class="font-16 color-grey padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr />
      </div>
    </div>
    <div class="row xs-text-center">
      <div class="col-md-6 col-sm-12 col-xs-12 margin_top_30">
        <p class="primary_text font-18 font-weight-700">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        <p class="color-grey font-16 padding_tb_20">Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        <ul class="powerful_list">
          <li><a href="#">Domain Register</a></li>
          <li><a href="#">Shared Hosting</a></li>
          <li><a href="#">Reseller Hosting</a></li>
          <li><a href="#">VPS Hosting</a></li>
          <li><a href="#">Dedicated Server</a></li>
        </ul>
      </div>
      <div class="col-md-6 margin_top_30 hidden-sm hidden-xs">
        <div class="powerful-banner"><img src="images/powerful-banner.jpg" alt="" title="" /></div>
      </div>
    </div>
  </div>
</section>

<!-- ========== Powerful Industries Section End ========== --> 

<!-- ========== Footer Section Start ========== -->


<footer>
<?php include("includes/footer.php");?>
</footer>

<!-- ========== Footer Section End ========== --> 

<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 

<!-- Counter JavaScript --> 
<script type="text/javascript" src="js/jquery.waypoints.min.js"></script> 
<script type="text/javascript" src="js/jquery.counterup.min.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 



</body>
</html>
