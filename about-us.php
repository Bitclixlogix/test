<?php
session_start();
include('configuration.php');
include("includes/common_function.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo PROJECT_TITLE;?></title>

<!-- Favicon -->
<link rel="icon" href="images/favicon.png">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="all" />

<!-- Font Awesome CSS -->
<script src="https://use.fontawesome.com/baa5d86801.js"></script>

<!-- Main Template CSS -->
<link rel="stylesheet" href="css/style.css" media="all" />
<link rel="stylesheet" href="css/color/default.css" media="all" id="colors" />
</head>

<body>

<!-- ========== Header Section Start ========== -->

<header>
  <?php include("includes/header.php");?>
</header>

<!-- ========== Header Section End ========== --> 

<!-- ========== Banner Section Start ========== -->

<div class="pagemain_banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h1 class="color-white font-50 font-weight-400 text-uppercase xs-text-center">About Next Host</h1>
      </div>
    </div>
  </div>
</div>

<!-- ========== Banner Section End ========== --> 

<!-- ========== About Company Section Start ========== -->

<section class="powerful_industries">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <h2 class="font-32 color-dark-grey no_padding no_margin">Our Company</h2>
        <p class="font-16 color-grey padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr />
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12 margin_top_30 xs-text-center">
        <p class="primary_text font-18 font-weight-700">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        <p class="color-grey font-16 padding_top_20">Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.</p>
        <p class="color-grey font-16 padding_top_20">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>
      </div>
      <div class="col-md-6 margin_top_30 hidden-sm hidden-xs">
        <div class="powerful-banner"><img src="images/about-company.jpg" alt="" title="" /></div>
      </div>
    </div>
  </div>
</section>

<!-- ========== About Company Section End ========== --> 

<!-- ========== Why Choose Section Start ========== -->

<section class="background_grey text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h2 class="font-32 color-dark-grey no_padding no_margin">How it Works</h2>
        <p class="font-16 color-grey padding_top_10">Lorem ipsum dolor sit amet, consectetur adipiscing</p>
        <hr />
      </div>
    </div>
    <div class="row margin_bottom_40">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="why_box2 margin_top_30">
          <div class="font-32"><i class="fa fa-pencil-square" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Consult</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="why_box2 margin_top_30">
          <div class="font-32"><i class="fa fa-briefcase" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Create</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="why_box2 margin_top_30">
          <div class="font-32"><i class="fa fa-compass" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Develop</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="why_box2 margin_top_30">
          <div class="font-32"><i class="fa fa-check-circle" aria-hidden="true"></i></div>
          <h4 class="font-22 color-dark-grey padding_tb_20">Release</h4>
          <p class="color-grey font-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ========== Why Choose Section End ========== --> 

<!-- ========== Counter Section Start ========== -->

<section id="counter" class="counter text-center">
  <div class="main_counter_area">
    <div class="container">
      <div class="row">
        <div class="main_counter_content">
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="single_counter padding_tb_20 counter-hover"><i class="fa fa-tasks font-32" aria-hidden="true"></i>
              <h2 class="statistic-counter padding_tb_20">100</h2>
              <p>Projects</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="single_counter padding_tb_20 counter-hover margin-xs-top-50"><i class="fa fa-users font-32" aria-hidden="true"></i>
              <h2 class="statistic-counter padding_tb_20">400</h2>
              <p>Clients</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="single_counter padding_tb_20 counter-hover margin-xs-top-50"><i class="fa fa-trophy font-32" aria-hidden="true"></i>
              <h2 class="statistic-counter padding_tb_20">312</h2>
              <p>Awards</p>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="single_counter padding_tb_20 counter-hover margin-xs-top-50"><i class="fa fa-globe font-32" aria-hidden="true"></i>
              <h2 class="statistic-counter padding_tb_20">480</h2>
              <p>Domains</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- ========== Footer Section Start ========== -->
<footer>
<?php include("includes/footer.php");?>
</footer>
<!-- ========== Footer Section End ========== --> 

<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 

<!-- Counter JavaScript --> 
<script type="text/javascript" src="js/jquery.waypoints.min.js"></script> 
<script type="text/javascript" src="js/jquery.counterup.min.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 


</body>
</html>
