<?php
session_start();
include('configuration.php');
include("includes/common_function.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<title><?php echo PROJECT_TITLE;?></title>

<!-- Favicon -->
<link rel="icon" href="images/favicon.png">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="all" />

<!-- Font Awesome CSS -->
<script src="https://use.fontawesome.com/baa5d86801.js"></script>

<!-- Main Template CSS -->
<link rel="stylesheet" href="css/style.css" media="all" />
<link rel="stylesheet" href="css/color/default.css" media="all" id="colors" />
</head>

<body>

<!-- ========== Header Section Start ========== -->

<header>
  <?php include("includes/header.php");?>
</header>

<!-- ========== Header Section End ========== --> 

<!-- ========== Banner Section Start ========== -->

<div class="pagemain_banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <h1 class="color-white font-50 font-weight-400 text-uppercase xs-text-center">Privacy Policy</h1>
      </div>
    </div>
  </div>
</div>

<!-- ========== Banner Section End ========== --> 

<!-- ========== About Company Section Start ========== -->

<section class="powerful_industries">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 text-center">
        <h2 class="font-32 color-dark-grey no_padding no_margin">Privacy Policy</h2>        
        <hr />
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 margin_top_30" style="text-align:justify;">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
         Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Adipiscing elit, sed do eiusmo tempor incididunt ut labore. Lorem ipsum dolor sit amet, incididunt ut labore. Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmo tempor incididunt ut labore.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisiLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et doloremagna aliqua. Ut nim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
      </div>
      
    </div>
  </div>
</section>

<!-- ========== About Company Section End ========== --> 

<!-- ========== Why Choose Section Start ========== -->



<!-- ========== Why Choose Section End ========== --> 

<!-- ========== Counter Section Start ========== -->



<!-- ========== Footer Section Start ========== -->
<footer>
<?php include("includes/footer.php");?>
</footer>
<!-- ========== Footer Section End ========== --> 

<!-- Bootstrap core JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 

<!-- Counter JavaScript --> 
<script type="text/javascript" src="js/jquery.waypoints.min.js"></script> 
<script type="text/javascript" src="js/jquery.counterup.min.js"></script> 
<script type="text/javascript" src="js/main.js"></script> 


</body>
</html>
