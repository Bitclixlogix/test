  <div class="container">
    <!--<div class="footer_middle">
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="footer_heading color-white font-26 margin_bottom_20 padding_tb_10">Hosting</div>
          <ul class="footer_menu">
            <li><a href="shared-hosting.html">Shared Hosting</a></li>
            <li><a href="reseller-hosting.html">Reseller Hosting</a></li>
            <li><a href="dedicated-server.html">Dedicated Server</a></li>
            <li><a href="vps-hosting.html">VPS Hosting</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="footer_heading color-white font-26 margin_bottom_20 padding_tb_10">Domains</div>
          <ul class="footer_menu">
            <li><a href="register-domain.html">Register Domain</a></li>
            <li><a href="#">Transfer Domain</a></li>
            <li><a href="#">Domain Pricing</a></li>
            <li><a href="#">Domain Promos</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="footer_heading color-white font-26 margin_bottom_20 padding_tb_10 margin-sm">Quick Links</div>
          <ul class="footer_menu">
            <li><a href="index.html">Home</a></li>
            <li><a href="about-us.html">About Us</a></li>
            <li><a href="contact-us.html">Contact Us</a></li>
            <li><a href="#l">Testimonial</a></li>
          </ul>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="footer_heading color-white font-26 margin_bottom_20 padding_tb_10 margin-sm">Others</div>
          <ul class="footer_menu">
            <li><a href="#">Login</a></li>
            <li><a href="#l">Register</a></li>
            <li><a href="#">Support</a></li>
            <li><a href="#">Blog</a></li>
          </ul>
        </div>
      </div>
    </div>-->
    <div class="row">
      <div class="copyright padding_top_20 margin_top_20">
        <div class="col-md-6 col-sm-6 col-xs-12 color-grey">&copy 2020, Next Host, All rights reserved</div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <ul class="text-right">
            <li><a href="privacy-policy.php">Privacy Policy</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>