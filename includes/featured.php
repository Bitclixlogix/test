<section class="pt-xs-30">
  
      <div class="ser-feature-block center-sm">
        <div class="row">
          <div class="col-md-4 col-xs-12 feature-box-main">
            <div class="feature-box feature1">
              <div class="ser-title">HASSLE-FREE REPLACEMENT</div>
              <div class="ser-subtitle">FREE 7-DAY EASY REPLACEMENT POLICY ON UDDOINDIA.COM</div>
            </div>
          </div>
          <div class="col-md-4 col-xs-12 feature-box-main">
            <div class="feature-box feature2">
              <div class="ser-title">UDDO INDIA PROMISE</div>
              <div class="ser-subtitle">OUR BUYER PROTECTION COVERS YOUR PURCHASE FROM CLICK TO DELIVERY.</div>
            </div>
          </div>
          <div class="col-md-4 col-xs-12 feature-box-main">
            <div class="feature-box feature3">
              <div class="ser-title">NEED ASSISTANCE</div>
              <div class="ser-subtitle">WE DON'T HIDE BEHIND TECHNICAL WALLS. WE'RE OPEN 6-DAYS TO HELP YOU.</div>
            </div>
          </div>
          
        </div>
      </div>
   
  </section>
  <div class="clearfix"></div>