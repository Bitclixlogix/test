   <!-- block category -->
                <div class="block left-module">
                    <p class="title_block">Product types</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-category">
                            <div class="layered-content">
                                <ul class="tree-menu">
                                    <li class="active">
                                        <span></span><a href="#">Car Polis & Waxes</a>
                                        <!--<ul>
                                            <li><span></span><a href="#">Car Shampoo</a></li>
                                            <li><span></span><a href="#">Water Wiper</a></li>
                                            <li><span></span><a href="#">Wheel & Tyre</a></li>
                                            <li><span></span><a href="#">Window & Glass</a></li>
                                            <li><span></span><a href="#">Wipes</a></li>
                                            <li><span></span><a href="#">Battery</a></li>
                                            <li><span></span><a href="#"><span></span>Bluetooth mice</a></li>
                                        </ul>-->
                                    </li>
                                    <li><span></span><a href="#">Capacitor</a></li>
                                    <li><span></span><a href="#">Car Shampoo</a></li>
                                    <li><span></span><a href="#">Water Wiper </a></li>
                                    <li><span></span><a href="#">Wheel & Tyre</a></li>
                                    <li><span></span><a href="#">Window & Glass</a></li>
                                    <li><span></span><a href="#">Wipes</a></li>
                                    <li><span></span><a href="#">Battery</a></li>
                                    <li><span></span><a href="#">Bluetooth mice</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- ./layered -->
                    </div>
                </div>
                <!-- ./block category  -->
                <!-- block filter -->
                <div class="block left-module">
                    <p class="title_block">Filter selection</p>
                    <div class="block_content">
                        <!-- layered -->
                        <div class="layered layered-filter-price">
                            <!-- filter categgory -->
                            <div class="layered_subtitle">CATEGORIES</div>
                            <div class="layered-content">
                                <ul class="check-box-list">
                                    <li>
                                        <input type="checkbox" id="c1" name="cc" />
                                        <label for="c1">
                                        <span class="button"></span>
                                        Capacitor<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c2" name="cc" />
                                        <label for="c2">
                                        <span class="button"></span>
                                        Battery<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c3" name="cc" />
                                        <label for="c3">
                                        <span class="button"></span>
                                        Car Shampoo<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c4" name="cc" />
                                        <label for="c4">
                                        <span class="button"></span>
                                       Wipes<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c5" name="cc" />
                                        <label for="c5">
                                        <span class="button"></span>
                                        Wheel & Tyre<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c6" name="cc" />
                                        <label for="c6">
                                        <span class="button"></span>
                                        Water Wiper<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c7" name="cc" />
                                        <label for="c7">
                                        <span class="button"></span>
                                        Window & Glass<span class="count">(10)</span>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="c8" name="cc" />
                                        <label for="c8">
                                        <span class="button"></span>
                                        Bluetooth mice<span class="count">(10)</span>
                                        </label>
                                    </li>
                                </ul>
                            </div> 
                            <!-- ./filter categgory -->
                            <!-- filter price -->
                            
                            
                            <!-- ./filter price -->
                            <!-- filter color -->
                            
                            
                            <!-- ./filter color -->
                            <!-- ./filter brand -->
                            
                            
                            <!-- ./filter brand -->
                            <!-- ./filter size -->
                           
                            
                            <!-- ./filter size -->
                        </div>
                        <!-- ./layered -->

                    </div>
                </div>
                <!-- ./block filter  -->
                
                <!-- left silide -->
                
                <!--./left silde-->
                <!-- SPECIAL -->
                
                <!-- ./SPECIAL -->
                <!-- TAGS -->
                
                <!-- ./TAGS -->
                <!-- Testimonials -->
                <div class="block left-module">
                    <p class="title_block">Testimonials</p>
                    <div class="block_content">
                        <ul class="testimonials owl-carousel" data-loop="true" data-nav = "false" data-margin = "30" data-autoplayTimeout="1000" data-autoplay="true" data-autoplayHoverPause = "true" data-items="1">
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                            <li>
                                <div class="client-mane">Roverto & Maria</div>
                                <div class="client-avarta">
                                    <img src="assets/data/testimonial.jpg" alt="client-avarta">
                                </div>
                                <div class="testimonial">
                                    "Your product needs to improve more. To suit the needs and update your image up"
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- ./Testimonials -->
