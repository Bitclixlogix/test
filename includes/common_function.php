<?php
$dayarr=array('1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,'10'=>10,'11'=>11,'12'=>12,'13'=>13,'14'=>14,'15'=>15,'16'=>16,'17'=>17,'18'=>18,'19'=>19,'20'=>20,'21'=>21,'22'=>22,'23'=>23,'24'=>24,'25'=>25,'26'=>26,'27'=>27,'28'=>28,'29'=>29,'30'=>30,'31'=>31);

$monarr=array('01'=>"JAN",'02'=>"FEB",'03'=>"MAR",'04'=>"APR",'05'=>"MAY",'06'=>"JUN",'07'=>"JUL",'08'=>"AUG",'09'=>"SEP",'10'=>"OCT",'11'=>"NOV",'12'=>"DEC");

$qtyrate=array('0'=>'Rejected','1'=>'Poor','2'=>'Average','3'=>'Good','4'=>'Great','5'=>'Outstanding');

//========== Show pagination details ============
	function showpagination($targetpage,$total,$page)
	{
		//echo $targetpage."==".$total."==".$page;
		//die;
		$pg1=1;
		$pg2=2;
		
		// How many adjacent pages should be shown on each side?
		$adjacents = 2;
		
		global $limitn;
		
		//echo $total."==".$limit;
		//die;
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total/$limitn);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
		
		
		
		$pagination = "";
		/*echo $lastpage;
		die;*/
		if($lastpage > 1)
		{	
			 $pagination .= "<div class=\"pagination\">";
			//previous button
			if ($page > 1) 
			{
				$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$prev.'\');">&laquo; Previous</a>';
				
			}
			else
			{
				//$pagination.= "<span class=\"disabled\">First</span>";	
				$pagination.= "<span class=\"disabled\">&laquo; Previous</span>";	
			}
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						 $pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
				}
			}
			elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
					}
					$pagination.= "...";
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lpm1.'\');">'.$lpm1.'</a>';
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lastpage.'\');">'.$lastpage.'</a>';	
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg1.'\');">1</a>';
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg2.'\');">2</a>';
					$pagination.= "...";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';					
					}
					$pagination.= "...";
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lpm1.'\');">'.$lpm1.'</a>';
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$lastpage.'\');">'.$lastpage.'</a>';
					
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg1.'\');">1</a>';
					//$pagination.= '<a href="#" onclick="getpage('.$targetpage.'\''.$pg2.'\');">2</a>';
					$pagination.= "...";
					for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<span class=\"current\">$counter</span>";
						else
							$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$counter.'\');">'.$counter.'</a>';
							
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
			{
				$next_page = $page+1;
				
				$pagination.='<a href="#" onclick="getpage('.$targetpage.'\''.$next_page.'\');">Next &raquo;</a>';
				
			}
			else
			{
				$pagination.= "<span class=\"disabled\">Next &raquo;</span>";
				//$pagination.= "<span class=\"disabled\">Last </span>";
			}
			$pagination.= "</div>\n";		
		}
		//echo $pagination;
		//die;
		

		return $pagination;
	}


//============= Page url redirection function ==============
function urlredirect($url)
{
	if (headers_sent()) {
		echo "<script>document.location.href='$url';</script>\n";
	} else {
		@ob_end_clean(); // clear output buffer
		header( 'HTTP/1.1 301 Moved Permanently' );
		header( "Location: ". $url );
	}
}
//======== function check login user =======
function check_userlogin_menu()
{
	global $db;
	
	if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id']))
	{
		url_redirect('index.php');
	}
	else
	{
		//echo "<pre>";
		//print_r($_SERVER[]);
		//echo substr($_SERVER["SCRIPT_NAME"],11);
		//die;
		
		if($_SERVER["SERVER_NAME"]=="localhost")
				$urlname=substr($_SERVER["SCRIPT_NAME"],11);
		else
				$urlname=substr($_SERVER["SCRIPT_NAME"],1);
				
		//========= runtime check=========
		$sqlusr = "select * from member where uniqID ='".$_SESSION['uniqID']."'";		
		$rowusr = $db->Execute($sqlusr);
		
		$sqlchk = "select * from tbl_menudetails where accessby like '%".$rowusr->fields['access_id']."%'";
		$sqlchk .=" and menu_url='".$urlname."' and  status ='0'";
		$rowcheck = $db->Execute($sqlchk);
		
		$totalchk= $rowcheck->RecordCount();
		
		if($totalchk>0)
		{			
		}
		else
		{
			if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))
			{
				session_unregister($_SESSION['user_id']);
				session_unregister($_SESSION['access_id']);
				session_unregister($_SESSION['username']);
				session_unregister($_SESSION['name']);
				session_unregister($_SESSION['uniqID']);	
				session_unregister($_SESSION['status']);
				
				session_destroy();
			}
			url_redirect('index.php');
		}
		
	}
}

//======== function check login user =======
function check_admlogin()
{
	global $db;
	
	if(!isset($_SESSION['reg_uniqID']) && empty($_SESSION['reg_uniqID']))
	{
		url_redirect('index.php');
	}	
}


function rating_value($reqid)
{
	if($reqid=="1")
	{
	echo '<img src="img/rating.jpg"/>';	
	}
	else if($reqid=="2")
	{
	echo '<img src="img/rating.jpg"/><img src="img/rating.jpg"/>';
	}
	else if($reqid=="3")
	{
	echo '<img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/>';
	}
	else if($reqid=="4")
	{
	echo '<img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/>';
	}
	else if($reqid=="5")
	{
		echo '<img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/><img src="img/rating.jpg"/>';
	}
	
	
}




function order_userunid($reqid)
{
	global $db;
	$inch_qry = "select * from   tbl_order_confirm_details where order_no ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["user_unid"];
}

function regname_user($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_register_user where user_unid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["first_name"];
}


function productacode($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["product_code"];
}

//=========== get product price ==== 
function productnamesh($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["product_name"];
}


function productnsh($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["product_name"];
}

//=========== get product price ==== 
function productdescsh($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["descp"];
}

function manufacture_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_manufacture_details where manu_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["manufacture_name"];
}


//=========== get product price ==== 
function productprice($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["rate"];
}

function shipping_charges($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["shipping_charges"];
}

//============ Country  Name ==========
function mcategory_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category_main where catid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cat_name"];
}

//============ Country  Name ==========
function catmainid($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category where catid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["maincat_id"];
}

//============ Country  Name ==========
function category_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category where catid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cat_name"];
}


//===== sub category name ==============
function subcategory_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category_sub where subcatid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cat_name"];
}

//===== subto sub category name ==============
function subtosubcategory_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_category_sub_to_sub where subsubcatid ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["cat_name"];
}


function product_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_subcategory where subcat_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["subcategory_name"];
}




/*
//============ Country  Name ==========
function country_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_country where country_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["country_name"];
}

//============ Country  Name ==========
function state_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_state where state_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["state_name"];
}

//============ Country  Name ==========
function city_name($reqid)
{
	global $db;
	$inch_qry = "select * from  tbl_city where city_id ='".$reqid."' ";
	$resch = $db->Execute($inch_qry);
	return $resch->fields["city_name"];
}

*/





?>