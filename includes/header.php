  <div class="top_bar padding_tb_20">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="logo"><a href="index.php"><img src="images/logo.png" alt="" title="" /></a></div>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">
          <ul>
            <li><a href="#" class="border-radius-3"><i class="fa fa-phone" aria-hidden="true"></i> 91+ 784 5963 587</a></li>
            <li><a href="mailto:info@rctheme.com" class="border-radius-3"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span class=" hidden-xs">info@rctheme.com</span></a></li>
			<li><a href="#" class="border-radius-3"><i class="fa fa-key" aria-hidden="true"></i> <span class="">Login</span></a></li>
            <li><a href="#" class="border-radius-3"><i class="fa fa-sign-in" aria-hidden="true"></i> <span class="">Register</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <nav class="navbar navbar-bootsnipp animate" role="navigation">
    <div class="container">
      <div class="row">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <!--<li class="active"> <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">Home <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="index.html" class="animate">Home Page 1</a></li>
                <li><a href="index-2.html" class="animate">Home Page 2</a></li>
              </ul>
            </li>-->
             <li><a href="index.php">Home</a></li>
           <!-- <li><a href="register-domain.html">Domain</a></li>
            <li><a href="shared-hosting.html">Shared Hosting</a></li>
            <li><a href="reseller-hosting.html">Reseller Hosting</a></li>
            <li><a href="dedicated-server.html">Dedicated Server</a></li>
            <li><a href="vps-hosting.html">VPS Hosting</a></li>-->
            <li><a href="about-us.php">About Us</a></li>
            <li><a href="contact-us.php">Contact Us</a></li>
          </ul>
        </div>
      </div>
    </div>
  </nav>